import 'package:flutter/material.dart';

class ThemeColors {
  static const textColor = Color(0xFF1C1C28);

  static const primary = primaryShade2;
  static const primaryShade1 = Color(0xFF4A2BED);
  static const primaryShade2 = Color(0xFF5e43ef);
  static const primaryShade5 = Color(0xFFd6d0fb);
  static const primaryShade6 = Color(0xFFeae7fd);
  static const primaryShade7 = Color(0xFFf3f1fe);

  static const light = Color(0xFFFFFFFF);
  static const lightShade1 = Color(0xFFF3F3F3);

  static const grayShade0 = Color(0xFF858585);
  static const grayShade1 = Color(0xFF62646C);
  static const grayShade3 = Color(0xFFAFB0B6);
  static const grayShade7 = Color(0xFFF7F7F8);
}

class ThemeConstants {
  static const inputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(100)),
    borderSide: BorderSide(
      width: 1.5,
      color: ThemeColors.lightShade1,
    ),
  );
  static const inputFocusedBorder = OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(100)),
    borderSide: BorderSide(
      width: 1.5,
      color: ThemeColors.primaryShade5,
    ),
  );
  static final inputErrorBorder = OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(100)),
    borderSide: BorderSide(
      width: 1.5,
      color: Colors.red.shade300,
    ),
  );
}
