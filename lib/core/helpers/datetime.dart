String jsonDateToString(int hour, int min) {
  if (hour < 12) {
    return 'AM $hour:$min';
  }
  return 'PM ${hour - 12}:$min';
}
