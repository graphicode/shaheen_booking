/// Generated file. Do not edit.
///
/// Original: lib/core/i18n
/// To regenerate, run: `dart run slang`
///
/// Locales: 2
/// Strings: 162 (81 per locale)
///
/// Built on 2024-04-06 at 13:53 UTC

// coverage:ignore-file
// ignore_for_file: type=lint

import 'package:flutter/widgets.dart';
import 'package:slang/builder/model/node.dart';
import 'package:slang_flutter/slang_flutter.dart';
export 'package:slang_flutter/slang_flutter.dart';

const AppLocale _baseLocale = AppLocale.en;

/// Supported locales, see extension methods below.
///
/// Usage:
/// - LocaleSettings.setLocale(AppLocale.en) // set locale
/// - Locale locale = AppLocale.en.flutterLocale // get flutter locale from enum
/// - if (LocaleSettings.currentLocale == AppLocale.en) // locale check
enum AppLocale with BaseAppLocale<AppLocale, Translations> {
	en(languageCode: 'en', build: Translations.build),
	ar(languageCode: 'ar', build: _StringsAr.build);

	const AppLocale({required this.languageCode, this.scriptCode, this.countryCode, required this.build}); // ignore: unused_element

	@override final String languageCode;
	@override final String? scriptCode;
	@override final String? countryCode;
	@override final TranslationBuilder<AppLocale, Translations> build;

	/// Gets current instance managed by [LocaleSettings].
	Translations get translations => LocaleSettings.instance.translationMap[this]!;
}

/// Method A: Simple
///
/// No rebuild after locale change.
/// Translation happens during initialization of the widget (call of t).
/// Configurable via 'translate_var'.
///
/// Usage:
/// String a = t.someKey.anotherKey;
/// String b = t['someKey.anotherKey']; // Only for edge cases!
Translations get t => LocaleSettings.instance.currentTranslations;

/// Method B: Advanced
///
/// All widgets using this method will trigger a rebuild when locale changes.
/// Use this if you have e.g. a settings page where the user can select the locale during runtime.
///
/// Step 1:
/// wrap your App with
/// TranslationProvider(
/// 	child: MyApp()
/// );
///
/// Step 2:
/// final t = Translations.of(context); // Get t variable.
/// String a = t.someKey.anotherKey; // Use t variable.
/// String b = t['someKey.anotherKey']; // Only for edge cases!
class TranslationProvider extends BaseTranslationProvider<AppLocale, Translations> {
	TranslationProvider({required super.child}) : super(settings: LocaleSettings.instance);

	static InheritedLocaleData<AppLocale, Translations> of(BuildContext context) => InheritedLocaleData.of<AppLocale, Translations>(context);
}

/// Method B shorthand via [BuildContext] extension method.
/// Configurable via 'translate_var'.
///
/// Usage (e.g. in a widget's build method):
/// context.t.someKey.anotherKey
extension BuildContextTranslationsExtension on BuildContext {
	Translations get t => TranslationProvider.of(this).translations;
}

/// Manages all translation instances and the current locale
class LocaleSettings extends BaseFlutterLocaleSettings<AppLocale, Translations> {
	LocaleSettings._() : super(utils: AppLocaleUtils.instance);

	static final instance = LocaleSettings._();

	// static aliases (checkout base methods for documentation)
	static AppLocale get currentLocale => instance.currentLocale;
	static Stream<AppLocale> getLocaleStream() => instance.getLocaleStream();
	static AppLocale setLocale(AppLocale locale, {bool? listenToDeviceLocale = false}) => instance.setLocale(locale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale setLocaleRaw(String rawLocale, {bool? listenToDeviceLocale = false}) => instance.setLocaleRaw(rawLocale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale useDeviceLocale() => instance.useDeviceLocale();
	@Deprecated('Use [AppLocaleUtils.supportedLocales]') static List<Locale> get supportedLocales => instance.supportedLocales;
	@Deprecated('Use [AppLocaleUtils.supportedLocalesRaw]') static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
	static void setPluralResolver({String? language, AppLocale? locale, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver}) => instance.setPluralResolver(
		language: language,
		locale: locale,
		cardinalResolver: cardinalResolver,
		ordinalResolver: ordinalResolver,
	);
}

/// Provides utility functions without any side effects.
class AppLocaleUtils extends BaseAppLocaleUtils<AppLocale, Translations> {
	AppLocaleUtils._() : super(baseLocale: _baseLocale, locales: AppLocale.values);

	static final instance = AppLocaleUtils._();

	// static aliases (checkout base methods for documentation)
	static AppLocale parse(String rawLocale) => instance.parse(rawLocale);
	static AppLocale parseLocaleParts({required String languageCode, String? scriptCode, String? countryCode}) => instance.parseLocaleParts(languageCode: languageCode, scriptCode: scriptCode, countryCode: countryCode);
	static AppLocale findDeviceLocale() => instance.findDeviceLocale();
	static List<Locale> get supportedLocales => instance.supportedLocales;
	static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
}

// translations

// Path: <root>
class Translations implements BaseTranslations<AppLocale, Translations> {
	/// Returns the current translations of the given [context].
	///
	/// Usage:
	/// final t = Translations.of(context);
	static Translations of(BuildContext context) => InheritedLocaleData.of<AppLocale, Translations>(context).translations;

	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	Translations.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.en,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <en>.
	@override final TranslationMetadata<AppLocale, Translations> $meta;

	/// Access flat map
	dynamic operator[](String key) => $meta.getTranslation(key);

	late final Translations _root = this; // ignore: unused_field

	// Translations
	String get wcontinue => 'Continue';
	String get save => 'Save Changes';
	late final _StringsAuthEn auth = _StringsAuthEn._(_root);
	late final _StringsProfileEn profile = _StringsProfileEn._(_root);
	String get editProfile => 'Edit Profile';
	String get searchLang => 'Search for Your Language';
	late final _StringsForgotPasswordEn forgotPassword = _StringsForgotPasswordEn._(_root);
	late final _StringsHomeEn home = _StringsHomeEn._(_root);
	String get choosePlace => 'Search for Destination';
	String get search => 'Search';
	String get confirm => 'Confirm';
	late final _StringsChooseDateEn chooseDate = _StringsChooseDateEn._(_root);
	late final _StringsFirstOpenEn firstOpen = _StringsFirstOpenEn._(_root);
}

// Path: auth
class _StringsAuthEn {
	_StringsAuthEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get login => 'Log In';
	String get pleaseLogin => 'Please Log In to Your Account';
	String get email => 'Email';
	String get putEmail => 'Enter Email';
	String get pass => 'Password';
	String get putPass => 'Enter Password';
	String get didUForgotPass => 'Forgot Password?';
	String get createAccount => 'Create Account';
	String get submit => 'Sign In';
	String get signInWith => 'Sign In With';
	String get signWithGoogle => 'Sign In With Google';
	String get signWithFacebook => 'Sign In With Facebook';
	String get newAccount => 'Create New Account';
	String get pleaseCreateAccountFirst => 'Please Create an Account First to Enjoy Features';
	String get name => 'Name';
	String get putName => 'Enter Your Name';
	String get phoneNumber => 'Phone Number';
	String get putPhoneNumber => 'Enter Phone Number';
	String get accountCreatedSuccess => 'Account Created Successfully';
	String get accountCreatedSuccessDetails => 'Congratulations, Your Account Has Been Successfully Completed\nA confirmation email has been sent to your email address.';
}

// Path: profile
class _StringsProfileEn {
	_StringsProfileEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get title => 'Profile';
	String get edit => 'Edit';
	String get mainLocation => 'Main Location';
	String get mainLocationDescription => 'Add your location or address';
	String get paymentMethod => 'Payment Method';
	String get paymentMethodDescription => 'Add payment method';
	String get currency => 'Currency';
	String get currencyDescription => 'Select currency based on your country';
	String get lang => 'Search Language';
	String get langDescription => 'Choose search language';
	String get history => 'Transaction History';
	String get historyDescription => 'Your transaction history';
	String get logout => 'Log Out';
}

// Path: forgotPassword
class _StringsForgotPasswordEn {
	_StringsForgotPasswordEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get forgotPassword => 'Forgot Password';
	String get description => 'Don\'t worry, we will send you a verification code to your email address. You can use it to reset your password';
	String get yourEmail => 'Enter Your Email';
	String get enterOtp => 'Reset Password';
	String get otpDescription => 'We have sent you a verification code to your email';
	String get resend => 'Resend';
	String get didNotSent => 'Did not receive reset code?';
	String get resetPasswordDescription => 'Reset Your Password';
	String get newPassword => 'New Password';
	String get passwordReenter => 'Confirm Password';
}

// Path: home
class _StringsHomeEn {
	_StringsHomeEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get famousPlaces => 'Famous Places';
	String hellowMr({required Object name}) => 'Hello ${name}';
	String get reserveNow => 'Reserve Now';
	String get enjoyYourTravelExperiance => 'Enjoy your unique travel experience';
	String get findYourWay => 'Find Your Way';
	String get whereToStay => 'Where to Stay';
	String get planeReserve => 'Flight Reservation';
	String get hotelReserve => 'Hotel Reservation';
	String get goAndBack => 'Round Trip';
	String get justGo => 'One Way';
	String get from => 'From';
	String get to => 'To';
	String get timeToArrive => 'Select Arrival Date';
	String get timeToBack => 'Select Departure Date';
	String get travelPlan => 'Flight Class';
	String get passengers => 'Passengers';
	String get adults => 'Adults';
	String get kids => 'Kids';
	late final _StringsHomeTravelPlansEn travelPlans = _StringsHomeTravelPlansEn._(_root);
	String get hotelGuests => 'Guests and Rooms';
	String get room => 'Room';
	String get distination => 'Destination';
	String get choose => 'Choose';
}

// Path: chooseDate
class _StringsChooseDateEn {
	_StringsChooseDateEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get date => 'Select Date and Time';
	String get time => 'Selected Time';
}

// Path: firstOpen
class _StringsFirstOpenEn {
	_StringsFirstOpenEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get hello => 'Hello';
	String get welcomeMessage => 'Please log in to enjoy\nour app\'s key features';
}

// Path: home.travelPlans
class _StringsHomeTravelPlansEn {
	_StringsHomeTravelPlansEn._(this._root);

	final Translations _root; // ignore: unused_field

	// Translations
	String get economy => 'Economy Class';
	String get featured => 'Featured';
	String get business => 'Business Class';
	String get firstClass => 'First Class';
	String get unspecified => 'Unspecified';
}

// Path: <root>
class _StringsAr implements Translations {
	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	_StringsAr.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.ar,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <ar>.
	@override final TranslationMetadata<AppLocale, Translations> $meta;

	/// Access flat map
	@override dynamic operator[](String key) => $meta.getTranslation(key);

	@override late final _StringsAr _root = this; // ignore: unused_field

	// Translations
	@override String get wcontinue => 'أستمرار';
	@override String get save => 'حفظ التغيرات';
	@override late final _StringsAuthAr auth = _StringsAuthAr._(_root);
	@override late final _StringsProfileAr profile = _StringsProfileAr._(_root);
	@override String get editProfile => 'تعديل الملف الشخصى';
	@override String get searchLang => 'ابحث عن اللغة الخاصة بك';
	@override late final _StringsForgotPasswordAr forgotPassword = _StringsForgotPasswordAr._(_root);
	@override late final _StringsHomeAr home = _StringsHomeAr._(_root);
	@override String get choosePlace => 'ابحث عن الوجهه';
	@override String get search => 'بحث';
	@override String get confirm => 'تأكيد';
	@override late final _StringsChooseDateAr chooseDate = _StringsChooseDateAr._(_root);
	@override late final _StringsFirstOpenAr firstOpen = _StringsFirstOpenAr._(_root);
}

// Path: auth
class _StringsAuthAr implements _StringsAuthEn {
	_StringsAuthAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get login => 'تسجيل الدخول';
	@override String get pleaseLogin => 'من فضلك سجل حسابك الخاص';
	@override String get email => 'البريد الإلكتروني';
	@override String get putEmail => 'أدخل البريد الإلكتروني';
	@override String get pass => 'كلمة المرور';
	@override String get putPass => 'أدخل كلمة المرور';
	@override String get didUForgotPass => 'هل نسيت كلمة المرور';
	@override String get createAccount => 'إنشاء حساب';
	@override String get submit => 'تسجيل';
	@override String get signInWith => 'سجل من خلال';
	@override String get signWithGoogle => 'سجل من خلال جوجل';
	@override String get signWithFacebook => 'سجل من خلال فيسبوك';
	@override String get newAccount => 'انشاء حساب جديد';
	@override String get pleaseCreateAccountFirst => 'من فضلك أنشاء حساب أولا قبل الأستمتاع بالمميزات';
	@override String get name => 'الأسم';
	@override String get putName => 'أدخل اأسمك';
	@override String get phoneNumber => 'رقم الهاتف';
	@override String get putPhoneNumber => 'أدخل رقم الهاتف';
	@override String get accountCreatedSuccess => 'تم أنشاء حسابك بنجاح';
	@override String get accountCreatedSuccessDetails => 'تهانينا, تم استكمال حسابك بنجاح\nتم أرسال رسالة الى بريك الألكترونى للتأكيد حسابك';
}

// Path: profile
class _StringsProfileAr implements _StringsProfileEn {
	_StringsProfileAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get title => 'الملف الشخصى';
	@override String get edit => 'تعديل';
	@override String get mainLocation => 'موقعك الرئيسي';
	@override String get mainLocationDescription => 'أضف موقعك او عنوانك الخاص بك';
	@override String get paymentMethod => 'طريقة الدفع';
	@override String get paymentMethodDescription => 'أضف طريقة دفع';
	@override String get currency => 'العملة';
	@override String get currencyDescription => 'حدد عملة حسب بلدك';
	@override String get lang => 'لغة البحث';
	@override String get langDescription => 'أختر لغة البحث';
	@override String get history => 'تاريخ المعاملة';
	@override String get historyDescription => 'تاريخ المعاملات الخاصة بك';
	@override String get logout => 'تسجيل الخروج';
}

// Path: forgotPassword
class _StringsForgotPasswordAr implements _StringsForgotPasswordEn {
	_StringsForgotPasswordAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get forgotPassword => 'نسيت كلمة المرور';
	@override String get description => 'لا تقلق سوف نرسل لك رمز التحقق على يريدك الألكترونى ستتمكن من خلالها أعادة ضبط كلمة المرور';
	@override String get yourEmail => 'ادخل البريد الإلكتروني الخاص بك';
	@override String get enterOtp => 'أعادة تعيين كلمة المرور';
	@override String get otpDescription => 'لقد أرسلنا لك رمز التحقق على بريدك الألكترونى';
	@override String get resend => 'أعادة الأرسال';
	@override String get didNotSent => 'لم يتم أرسال رمز أعادة التعيين؟';
	@override String get resetPasswordDescription => 'أعد تعيين كلمة مرور جديدة';
	@override String get newPassword => 'كلمة المرور الجديدة';
	@override String get passwordReenter => 'تأكيد كلمة المرور';
}

// Path: home
class _StringsHomeAr implements _StringsHomeEn {
	_StringsHomeAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get famousPlaces => 'اماكن مشهورة';
	@override String hellowMr({required Object name}) => 'مرحبا بك ${name}';
	@override String get reserveNow => 'احجز الآن';
	@override String get enjoyYourTravelExperiance => 'استمتع بتجربة السفر المميزة';
	@override String get findYourWay => 'ابحث عن وجهتك';
	@override String get whereToStay => 'أين تود المبيت';
	@override String get planeReserve => 'حجز طيران';
	@override String get hotelReserve => 'حجز فندق';
	@override String get goAndBack => 'ذهاب و عودة';
	@override String get justGo => 'ذهاب فقط';
	@override String get from => 'من';
	@override String get to => 'الي';
	@override String get timeToArrive => 'اختر تاريخ الوصول';
	@override String get timeToBack => 'اختر تاريخ المغادرة';
	@override String get travelPlan => 'درجة الرحلة';
	@override String get passengers => 'الركاب';
	@override String get adults => 'بالغون';
	@override String get kids => 'أطفال';
	@override late final _StringsHomeTravelPlansAr travelPlans = _StringsHomeTravelPlansAr._(_root);
	@override String get hotelGuests => 'الضيوف والغرف';
	@override String get room => 'غرفة';
	@override String get distination => 'الوجهة';
	@override String get choose => 'أختر';
}

// Path: chooseDate
class _StringsChooseDateAr implements _StringsChooseDateEn {
	_StringsChooseDateAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get date => 'حدد التاريخ و الوقت';
	@override String get time => 'الوقت المحدد';
}

// Path: firstOpen
class _StringsFirstOpenAr implements _StringsFirstOpenEn {
	_StringsFirstOpenAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get hello => 'مرحبا';
	@override String get welcomeMessage => 'نرجو منك تسجيل الدخول للأستمتاع\n بأهم مميزات تطبيقنا';
}

// Path: home.travelPlans
class _StringsHomeTravelPlansAr implements _StringsHomeTravelPlansEn {
	_StringsHomeTravelPlansAr._(this._root);

	@override final _StringsAr _root; // ignore: unused_field

	// Translations
	@override String get economy => 'الدرجة الاقتصادية ';
	@override String get featured => 'المميزة';
	@override String get business => 'درجة الأعمال';
	@override String get firstClass => 'الدرجة الأولى';
	@override String get unspecified => 'غير محدد';
}

/// Flat map(s) containing all translations.
/// Only for edge cases! For simple maps, use the map function of this library.

extension on Translations {
	dynamic _flatMapFunction(String path) {
		switch (path) {
			case 'wcontinue': return 'Continue';
			case 'save': return 'Save Changes';
			case 'auth.login': return 'Log In';
			case 'auth.pleaseLogin': return 'Please Log In to Your Account';
			case 'auth.email': return 'Email';
			case 'auth.putEmail': return 'Enter Email';
			case 'auth.pass': return 'Password';
			case 'auth.putPass': return 'Enter Password';
			case 'auth.didUForgotPass': return 'Forgot Password?';
			case 'auth.createAccount': return 'Create Account';
			case 'auth.submit': return 'Sign In';
			case 'auth.signInWith': return 'Sign In With';
			case 'auth.signWithGoogle': return 'Sign In With Google';
			case 'auth.signWithFacebook': return 'Sign In With Facebook';
			case 'auth.newAccount': return 'Create New Account';
			case 'auth.pleaseCreateAccountFirst': return 'Please Create an Account First to Enjoy Features';
			case 'auth.name': return 'Name';
			case 'auth.putName': return 'Enter Your Name';
			case 'auth.phoneNumber': return 'Phone Number';
			case 'auth.putPhoneNumber': return 'Enter Phone Number';
			case 'auth.accountCreatedSuccess': return 'Account Created Successfully';
			case 'auth.accountCreatedSuccessDetails': return 'Congratulations, Your Account Has Been Successfully Completed\nA confirmation email has been sent to your email address.';
			case 'profile.title': return 'Profile';
			case 'profile.edit': return 'Edit';
			case 'profile.mainLocation': return 'Main Location';
			case 'profile.mainLocationDescription': return 'Add your location or address';
			case 'profile.paymentMethod': return 'Payment Method';
			case 'profile.paymentMethodDescription': return 'Add payment method';
			case 'profile.currency': return 'Currency';
			case 'profile.currencyDescription': return 'Select currency based on your country';
			case 'profile.lang': return 'Search Language';
			case 'profile.langDescription': return 'Choose search language';
			case 'profile.history': return 'Transaction History';
			case 'profile.historyDescription': return 'Your transaction history';
			case 'profile.logout': return 'Log Out';
			case 'editProfile': return 'Edit Profile';
			case 'searchLang': return 'Search for Your Language';
			case 'forgotPassword.forgotPassword': return 'Forgot Password';
			case 'forgotPassword.description': return 'Don\'t worry, we will send you a verification code to your email address. You can use it to reset your password';
			case 'forgotPassword.yourEmail': return 'Enter Your Email';
			case 'forgotPassword.enterOtp': return 'Reset Password';
			case 'forgotPassword.otpDescription': return 'We have sent you a verification code to your email';
			case 'forgotPassword.resend': return 'Resend';
			case 'forgotPassword.didNotSent': return 'Did not receive reset code?';
			case 'forgotPassword.resetPasswordDescription': return 'Reset Your Password';
			case 'forgotPassword.newPassword': return 'New Password';
			case 'forgotPassword.passwordReenter': return 'Confirm Password';
			case 'home.famousPlaces': return 'Famous Places';
			case 'home.hellowMr': return ({required Object name}) => 'Hello ${name}';
			case 'home.reserveNow': return 'Reserve Now';
			case 'home.enjoyYourTravelExperiance': return 'Enjoy your unique travel experience';
			case 'home.findYourWay': return 'Find Your Way';
			case 'home.whereToStay': return 'Where to Stay';
			case 'home.planeReserve': return 'Flight Reservation';
			case 'home.hotelReserve': return 'Hotel Reservation';
			case 'home.goAndBack': return 'Round Trip';
			case 'home.justGo': return 'One Way';
			case 'home.from': return 'From';
			case 'home.to': return 'To';
			case 'home.timeToArrive': return 'Select Arrival Date';
			case 'home.timeToBack': return 'Select Departure Date';
			case 'home.travelPlan': return 'Flight Class';
			case 'home.passengers': return 'Passengers';
			case 'home.adults': return 'Adults';
			case 'home.kids': return 'Kids';
			case 'home.travelPlans.economy': return 'Economy Class';
			case 'home.travelPlans.featured': return 'Featured';
			case 'home.travelPlans.business': return 'Business Class';
			case 'home.travelPlans.firstClass': return 'First Class';
			case 'home.travelPlans.unspecified': return 'Unspecified';
			case 'home.hotelGuests': return 'Guests and Rooms';
			case 'home.room': return 'Room';
			case 'home.distination': return 'Destination';
			case 'home.choose': return 'Choose';
			case 'choosePlace': return 'Search for Destination';
			case 'search': return 'Search';
			case 'confirm': return 'Confirm';
			case 'chooseDate.date': return 'Select Date and Time';
			case 'chooseDate.time': return 'Selected Time';
			case 'firstOpen.hello': return 'Hello';
			case 'firstOpen.welcomeMessage': return 'Please log in to enjoy\nour app\'s key features';
			default: return null;
		}
	}
}

extension on _StringsAr {
	dynamic _flatMapFunction(String path) {
		switch (path) {
			case 'wcontinue': return 'أستمرار';
			case 'save': return 'حفظ التغيرات';
			case 'auth.login': return 'تسجيل الدخول';
			case 'auth.pleaseLogin': return 'من فضلك سجل حسابك الخاص';
			case 'auth.email': return 'البريد الإلكتروني';
			case 'auth.putEmail': return 'أدخل البريد الإلكتروني';
			case 'auth.pass': return 'كلمة المرور';
			case 'auth.putPass': return 'أدخل كلمة المرور';
			case 'auth.didUForgotPass': return 'هل نسيت كلمة المرور';
			case 'auth.createAccount': return 'إنشاء حساب';
			case 'auth.submit': return 'تسجيل';
			case 'auth.signInWith': return 'سجل من خلال';
			case 'auth.signWithGoogle': return 'سجل من خلال جوجل';
			case 'auth.signWithFacebook': return 'سجل من خلال فيسبوك';
			case 'auth.newAccount': return 'انشاء حساب جديد';
			case 'auth.pleaseCreateAccountFirst': return 'من فضلك أنشاء حساب أولا قبل الأستمتاع بالمميزات';
			case 'auth.name': return 'الأسم';
			case 'auth.putName': return 'أدخل اأسمك';
			case 'auth.phoneNumber': return 'رقم الهاتف';
			case 'auth.putPhoneNumber': return 'أدخل رقم الهاتف';
			case 'auth.accountCreatedSuccess': return 'تم أنشاء حسابك بنجاح';
			case 'auth.accountCreatedSuccessDetails': return 'تهانينا, تم استكمال حسابك بنجاح\nتم أرسال رسالة الى بريك الألكترونى للتأكيد حسابك';
			case 'profile.title': return 'الملف الشخصى';
			case 'profile.edit': return 'تعديل';
			case 'profile.mainLocation': return 'موقعك الرئيسي';
			case 'profile.mainLocationDescription': return 'أضف موقعك او عنوانك الخاص بك';
			case 'profile.paymentMethod': return 'طريقة الدفع';
			case 'profile.paymentMethodDescription': return 'أضف طريقة دفع';
			case 'profile.currency': return 'العملة';
			case 'profile.currencyDescription': return 'حدد عملة حسب بلدك';
			case 'profile.lang': return 'لغة البحث';
			case 'profile.langDescription': return 'أختر لغة البحث';
			case 'profile.history': return 'تاريخ المعاملة';
			case 'profile.historyDescription': return 'تاريخ المعاملات الخاصة بك';
			case 'profile.logout': return 'تسجيل الخروج';
			case 'editProfile': return 'تعديل الملف الشخصى';
			case 'searchLang': return 'ابحث عن اللغة الخاصة بك';
			case 'forgotPassword.forgotPassword': return 'نسيت كلمة المرور';
			case 'forgotPassword.description': return 'لا تقلق سوف نرسل لك رمز التحقق على يريدك الألكترونى ستتمكن من خلالها أعادة ضبط كلمة المرور';
			case 'forgotPassword.yourEmail': return 'ادخل البريد الإلكتروني الخاص بك';
			case 'forgotPassword.enterOtp': return 'أعادة تعيين كلمة المرور';
			case 'forgotPassword.otpDescription': return 'لقد أرسلنا لك رمز التحقق على بريدك الألكترونى';
			case 'forgotPassword.resend': return 'أعادة الأرسال';
			case 'forgotPassword.didNotSent': return 'لم يتم أرسال رمز أعادة التعيين؟';
			case 'forgotPassword.resetPasswordDescription': return 'أعد تعيين كلمة مرور جديدة';
			case 'forgotPassword.newPassword': return 'كلمة المرور الجديدة';
			case 'forgotPassword.passwordReenter': return 'تأكيد كلمة المرور';
			case 'home.famousPlaces': return 'اماكن مشهورة';
			case 'home.hellowMr': return ({required Object name}) => 'مرحبا بك ${name}';
			case 'home.reserveNow': return 'احجز الآن';
			case 'home.enjoyYourTravelExperiance': return 'استمتع بتجربة السفر المميزة';
			case 'home.findYourWay': return 'ابحث عن وجهتك';
			case 'home.whereToStay': return 'أين تود المبيت';
			case 'home.planeReserve': return 'حجز طيران';
			case 'home.hotelReserve': return 'حجز فندق';
			case 'home.goAndBack': return 'ذهاب و عودة';
			case 'home.justGo': return 'ذهاب فقط';
			case 'home.from': return 'من';
			case 'home.to': return 'الي';
			case 'home.timeToArrive': return 'اختر تاريخ الوصول';
			case 'home.timeToBack': return 'اختر تاريخ المغادرة';
			case 'home.travelPlan': return 'درجة الرحلة';
			case 'home.passengers': return 'الركاب';
			case 'home.adults': return 'بالغون';
			case 'home.kids': return 'أطفال';
			case 'home.travelPlans.economy': return 'الدرجة الاقتصادية ';
			case 'home.travelPlans.featured': return 'المميزة';
			case 'home.travelPlans.business': return 'درجة الأعمال';
			case 'home.travelPlans.firstClass': return 'الدرجة الأولى';
			case 'home.travelPlans.unspecified': return 'غير محدد';
			case 'home.hotelGuests': return 'الضيوف والغرف';
			case 'home.room': return 'غرفة';
			case 'home.distination': return 'الوجهة';
			case 'home.choose': return 'أختر';
			case 'choosePlace': return 'ابحث عن الوجهه';
			case 'search': return 'بحث';
			case 'confirm': return 'تأكيد';
			case 'chooseDate.date': return 'حدد التاريخ و الوقت';
			case 'chooseDate.time': return 'الوقت المحدد';
			case 'firstOpen.hello': return 'مرحبا';
			case 'firstOpen.welcomeMessage': return 'نرجو منك تسجيل الدخول للأستمتاع\n بأهم مميزات تطبيقنا';
			default: return null;
		}
	}
}
