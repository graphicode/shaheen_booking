import 'package:get/get.dart';
import 'package:shaheen_booking/models/user.dart';
import 'package:shaheen_booking/services/storage.dart';

class AuthService extends GetxService {
  static AuthService get to => Get.find();
  Future<AuthService> init() async {
    user = StorageService.user;
    return this;
  }

  UserModel? user;
  static UserModel get currentUser => to.user!;
  static bool get loggedIn => to.user != null;
}
