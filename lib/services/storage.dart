import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shaheen_booking/models/currency.dart';
import 'package:shaheen_booking/models/user.dart';

class StorageService extends GetxService {
  static StorageService get to => Get.find();
  Future<StorageService> init() async {
    await GetStorage.init('shaheen_booking_storage');
    box = GetStorage('shaheen_booking_storage');
    return this;
  }

  late final GetStorage box;

  static String? get lang => to.box.read('lang');
  static set lang(String? l) => to.box.write('lang', l);

  static String? get token => to.box.read('token');
  static set token(String? l) => to.box.write('token', l);

  static UserModel? get user {
    final read = to.box.read('user');
    if (read == null) return null;
    return UserModel.fromJson(read);
  }

  static set user(UserModel? l) {
    final value = l?.toJson();
    value?.remove('token');
    to.box.write('user', value);
  }

  static String get locale => to.box.read('locale') ?? 'ar-AE';
  static set locale(String? l) => to.box.write('locale', l);

  static String get market => to.box.read('market') ?? 'AED';
  static set market(String? l) => to.box.write('market', l);

  static CurrencyModel get currency {
    var read = to.box.read('currency');
    if (read == null) {
      return CurrencyModel(
        code: 'AED',
        symbol: 'AED',
        decimalDigits: 2,
        decimalSeparator: '.',
        spaceBetweenAmountAndSymbol: true,
        thousandsSeparator: ',',
      );
    }
    return CurrencyModel.fromJson(read);
  }

  static set currency(CurrencyModel? l) => to.box.write('currency', l?.toJson());
}
