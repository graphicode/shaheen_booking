import 'package:get/get.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';

class SettingsService extends GetxService {
  Future<SettingsService> init() async {
    // final storedLocale = StorageService.lang;
    // if (storedLocale != null || kDebugMode) {
    //   LocaleSettings.setLocaleRaw(storedLocale ?? 'ar');
    // } else {
    LocaleSettings.useDeviceLocale();
    // }
    return this;
  }
}
