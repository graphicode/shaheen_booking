import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({
    super.key,
    this.border = true,
  });

  final bool border;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: Get.back,
      child: Container(
        padding: const EdgeInsets.all(9),
        decoration: BoxDecoration(
          borderRadius: border == false ? null : BorderRadius.circular(50),
          border: border == false ? null : Border.all(width: 1.5, color: const Color(0xFFF3F3F3)),
        ),
        child: SvgPicture.asset(
          'assets/icons/back_arrow.svg',
        ),
      ),
    );
  }
}
