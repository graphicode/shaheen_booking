import 'package:flutter/material.dart';

class HorizontalOrLine extends StatelessWidget {
  const HorizontalOrLine({
    super.key,
    required this.label,
    required this.height,
  });

  final String label;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 15.0),
          child: Divider(
            height: height,
            color: const Color(0xFFF3F3F3),
          ),
        ),
      ),
      Text(
        label,
        style: const TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w400,
        ),
      ),
      Expanded(
        child: Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 10.0),
          child: Divider(
            height: height,
            color: const Color(0xFFF3F3F3),
          ),
        ),
      ),
    ]);
  }
}
