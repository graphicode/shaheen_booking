import 'package:json_annotation/json_annotation.dart';
part 'currency.g.dart';

@JsonSerializable()
class CurrencyModel {
  final String code;
  final String symbol;
  final String thousandsSeparator;
  final String decimalSeparator;
  final bool spaceBetweenAmountAndSymbol;
  final int decimalDigits;

  CurrencyModel({
    required this.code,
    required this.decimalDigits,
    required this.decimalSeparator,
    required this.spaceBetweenAmountAndSymbol,
    required this.symbol,
    required this.thousandsSeparator,
  });

  factory CurrencyModel.fromJson(Map<String, dynamic> json) => _$CurrencyModelFromJson(json);
  Map<String, dynamic> toJson() => _$CurrencyModelToJson(this);
}
