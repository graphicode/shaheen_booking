// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrencyModel _$CurrencyModelFromJson(Map<String, dynamic> json) =>
    CurrencyModel(
      code: json['code'] as String,
      decimalDigits: json['decimalDigits'] as int,
      decimalSeparator: json['decimalSeparator'] as String,
      spaceBetweenAmountAndSymbol: json['spaceBetweenAmountAndSymbol'] as bool,
      symbol: json['symbol'] as String,
      thousandsSeparator: json['thousandsSeparator'] as String,
    );

Map<String, dynamic> _$CurrencyModelToJson(CurrencyModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'symbol': instance.symbol,
      'thousandsSeparator': instance.thousandsSeparator,
      'decimalSeparator': instance.decimalSeparator,
      'spaceBetweenAmountAndSymbol': instance.spaceBetweenAmountAndSymbol,
      'decimalDigits': instance.decimalDigits,
    };
