import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/models/user.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({super.key});

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  String? avatarFilePath;
  final name = TextEditingController(text: AuthService.currentUser.name);
  final email = TextEditingController(text: AuthService.currentUser.email);
  final phone = TextEditingController(text: AuthService.currentUser.phone);

  submit() async {
    var res = await DoRequest.post(
      '/profile/update',
      data: FormData.fromMap({
        'name': name.text,
        'email': email.text,
        'phone': phone.text,
        if (avatarFilePath != null && avatarFilePath!.isNotEmpty)
          'img': await MultipartFile.fromFile(avatarFilePath!),
      }),
    );
    res = await DoRequest.get('/profile');
    StorageService.user = AuthService.to.user = UserModel.fromJson(res.data!['result']);
    Get.rawSnackbar(message: 'Updated Successfully');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.all(21),
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 4),
            child: SizedBox(
              height: kToolbarHeight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Spacer(),
                  const Spacer(),
                  const Spacer(),
                  Text(
                    t.editProfile,
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  const Spacer(),
                  const Spacer(),
                  const CustomBackButton(),
                ],
              ),
            ),
          ),
          const SizedBox(height: 11),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () async {
              FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.image);
              if (result != null) {
                setState(() {
                  avatarFilePath = result.files.first.path;
                });
              }
            },
            child: avatarFilePath != null
                ? CircleAvatar(
                    radius: 45,
                    backgroundImage: FileImage(File(avatarFilePath!)),
                  )
                : CircleAvatar(
                    radius: 45,
                    backgroundImage: NetworkImage(AuthService.currentUser.img!),
                  ),
          ),
          const SizedBox(height: 10),
          ListTile(
            dense: true,
            title: Text(
              t.auth.name,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          TextFormField(
            controller: name,
            keyboardType: TextInputType.name,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              hintText: t.auth.putName,
              prefixIcon: SvgPicture.asset(
                'assets/icons/person.svg',
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          const SizedBox(height: 8),
          ListTile(
            dense: true,
            title: Text(
              t.auth.email,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          TextFormField(
            controller: email,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              hintText: t.auth.putEmail,
              prefixIcon: SvgPicture.asset(
                'assets/icons/mail.svg',
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          const SizedBox(height: 8),
          ListTile(
            dense: true,
            title: Text(
              t.auth.phoneNumber,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          TextFormField(
            controller: phone,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
              hintText: t.auth.putPhoneNumber,
              prefixIcon: SvgPicture.asset(
                'assets/icons/phone.svg',
                fit: BoxFit.scaleDown,
              ),
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            onPressed: submit,
            child: Text(t.save),
          )
        ],
      ),
    );
  }
}
