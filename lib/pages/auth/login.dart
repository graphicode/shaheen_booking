import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shaheen_booking/components/horizontal_or_line.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/models/user.dart';
import 'package:shaheen_booking/pages/auth/sign_up.dart';
import 'package:shaheen_booking/pages/home/home.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

import 'forgot_password/forgot_password.dart';

const List<String> scopes = <String>[
  'email',
];
GoogleSignIn googleSignIn = GoogleSignIn(
  scopes: scopes,
  // clientId: '213441865259-er9r6avgtduu7bp58up3mvo2ud6lnr9d.apps.googleusercontent.com',
);

class LoginController {
  final TextEditingController email = TextEditingController();
  final TextEditingController password = TextEditingController();

  void submit() async {
    StorageService.token = null;
    DoRequest.deleteToken();
    try {
      var res = await DoRequest.post('/auth/login', data: {
        "email": email.text,
        "password": password.text,
      });
      if (res.data!['success'] == false) {
        return;
      }
      StorageService.token = res.data!['result']['token'];
      DoRequest.setToken(StorageService.token ?? '');

      Get.rawSnackbar(message: res.data!['message']);
      res = await DoRequest.get('/profile');
      StorageService.user = AuthService.to.user = UserModel.fromJson(res.data!['result']);
      Get.offAll(() => const HomePage());
    } on DioExp catch (e) {
      Get.rawSnackbar(message: e.response!.data!['message']);
    } on Exception {
      Get.rawSnackbar(message: 'Something went wrong.');
    }
  }

  void submitGoogle() async {
    StorageService.token = null;
    DoRequest.deleteToken();

    final currentUser = await googleSignIn.signIn();
    try {
      if (currentUser == null) {
        return;
      }
      var res = await DoRequest.post('/v1/authsocialite/mobile/fill', data: {
        "name": currentUser.displayName,
        "email": currentUser.email,
        "provider": "google",
        "provider_id": currentUser.id,
        "provider_token": (await currentUser.authentication).accessToken
      });
      if (res.data!['success'] == false) {
        return;
      }
      StorageService.token = res.data!['result']['token'];
      DoRequest.setToken(StorageService.token ?? '');

      Get.rawSnackbar(message: res.data!['message']);
      res = await DoRequest.get('/profile');
      StorageService.user = AuthService.to.user = UserModel.fromJson(res.data!['result']);
      Get.offAll(() => const HomePage());
    } on DioExp catch (e) {
      Get.rawSnackbar(message: e.response!.data!['message']);
    } on Exception {
      Get.rawSnackbar(message: 'Something went wrong.');
    }
  }
}

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(LoginController());
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Text(
              t.auth.login,
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            Text(
              t.auth.pleaseLogin,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: ThemeColors.grayShade0,
              ),
            ),
            const SizedBox(height: 28),
            ListTile(
              dense: true,
              title: Text(
                t.auth.email,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              controller: c.email,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: t.auth.putEmail,
                prefixIcon: SvgPicture.asset(
                  'assets/icons/mail.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 8),
            ListTile(
              dense: true,
              title: Text(
                t.auth.pass,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              obscureText: true,
              controller: c.password,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                hintText: t.auth.putPass,
                suffixIcon: SvgPicture.asset(
                  'assets/icons/eye.svg',
                  fit: BoxFit.scaleDown,
                ),
                prefixIcon: SvgPicture.asset(
                  'assets/icons/lock.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                  child: Text(
                    t.auth.createAccount,
                    style: theme.textTheme.labelSmall?.copyWith(
                      color: ThemeColors.primaryShade2,
                    ),
                  ),
                  onPressed: () {
                    Get.to(() => const SignUpPage());
                  },
                ),
                TextButton(
                  child: Text(
                    t.auth.didUForgotPass,
                    style: theme.textTheme.labelSmall?.copyWith(
                      color: ThemeColors.primaryShade2,
                    ),
                  ),
                  onPressed: () {
                    Get.to(() => const ForgotPasswordPage());
                  },
                ),
              ],
            ),
            const SizedBox(height: 51),
            ElevatedButton(
              onPressed: c.submit,
              child: Text(t.auth.submit),
            ),
            HorizontalOrLine(
              height: 60,
              label: t.auth.signInWith,
            ),
            OutlinedButton(
              onPressed: c.submitGoogle,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(t.auth.signWithGoogle),
                  const SizedBox(width: 11),
                  SvgPicture.asset(
                    'assets/icons/google.svg',
                    fit: BoxFit.scaleDown,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 6),
            // OutlinedButton(
            //   onPressed: () {},
            //   child: Row(
            //     mainAxisSize: MainAxisSize.min,
            //     children: [
            //       Text(t.auth.signWithFacebook),
            //       const SizedBox(width: 11),
            //       SvgPicture.asset(
            //         'assets/icons/facebook.svg',
            //         fit: BoxFit.scaleDown,
            //       ),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
