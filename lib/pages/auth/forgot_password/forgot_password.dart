import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';

import 'otp_confirm.dart';

class ForgotPasswordController extends GetxController {
  final TextEditingController email = TextEditingController();

  void submit() async {
    try {
      final res = await DoRequest.post('/auth/forgot-password', data: {"email": email.text});
      if (res.data!['success'] == false) {
        return;
      }
      Get.to(() => const OtpConfirmPage());
    } on DioExp catch (e) {
      Get.rawSnackbar(message: e.response!.data!['message']);
    } on Exception {
      Get.rawSnackbar(message: 'Something went wrong.');
    }
  }
}

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(ForgotPasswordController());
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Text(
              t.forgotPassword.forgotPassword,
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            const SizedBox(height: 3),
            Text(
              t.forgotPassword.description,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: ThemeColors.grayShade0,
              ),
            ),
            const SizedBox(height: 28),
            ListTile(
              dense: true,
              title: Text(
                t.forgotPassword.yourEmail,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              controller: c.email,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: t.auth.putEmail,
                prefixIcon: SvgPicture.asset(
                  'assets/icons/mail.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 51),
            ElevatedButton(
              onPressed: c.submit,
              child: Text(t.wcontinue),
            ),
          ],
        ),
      ),
    );
  }
}
