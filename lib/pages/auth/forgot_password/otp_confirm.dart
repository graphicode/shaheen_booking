import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';

class OtpConfirmController extends GetxController {
  final TextEditingController email = TextEditingController();

  void submit() async {
    try {
      final res = await DoRequest.post('/auth/forgot-password', data: {"email": email.text});
      if (res.data!['success'] == false) {
        return;
      }
      // Get.offAll(() => const HomePage());
    } on DioExp catch (e) {
      Get.rawSnackbar(message: e.response!.data!['message']);
    } on Exception {
      Get.rawSnackbar(message: 'Something went wrong.');
    }
  }
}

class OtpConfirmPage extends StatelessWidget {
  const OtpConfirmPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(OtpConfirmController());
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Text(
              t.forgotPassword.enterOtp,
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            const SizedBox(height: 3),
            Text(
              t.forgotPassword.otpDescription,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: ThemeColors.grayShade0,
              ),
            ),
            const SizedBox(height: 28),
            OtpTextField(
              numberOfFields: 6,
              // borderColor: Color(0xFF512DA8),
              showFieldAsBox: true,
              onCodeChanged: (String code) {},
              onSubmit: (String verificationCode) {}, // end onSubmit
            ),
            const SizedBox(height: 51),
            ElevatedButton(
              onPressed: c.submit,
              child: Text(t.wcontinue),
            ),
          ],
        ),
      ),
    );
  }
}
