import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/models/currency.dart';
import 'package:shaheen_booking/services/storage.dart';

class CurrencyController extends GetxService {
  final langs = [].obs;

  void fetch() async {
    final res = await DoRequest.get('/v1/cultures/currencies');
    langs.value = res.data!['result'];
  }

  @override
  void onInit() {
    super.onInit();
    fetch();
  }
}

class CurrencyPage extends StatelessWidget {
  const CurrencyPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(CurrencyController());
    return Scaffold(
      body: c.langs.isEmpty
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(21),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 16, bottom: 4),
                    child: SizedBox(
                      height: kToolbarHeight,
                      child: Row(
                        children: [
                          Expanded(
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: t.searchLang,
                                enabledBorder: ThemeConstants.inputBorder.copyWith(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 11),
                          const CustomBackButton(
                            border: false,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => ListView.separated(
                        itemCount: c.langs.length,
                        separatorBuilder: (context, index) {
                          return const Divider(thickness: .3);
                        },
                        itemBuilder: (context, index) {
                          final cur = c.langs[index];
                          return ListTile(
                            leading: Text(cur['symbol']),
                            title: Text(cur['code']),
                            onTap: () {
                              StorageService.currency = CurrencyModel.fromJson(cur);
                              Get.back();
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
