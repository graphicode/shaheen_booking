import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/services/storage.dart';

class ChangeLangController extends GetxService {
  final langs = [].obs;

  void fetch() async {
    final res = await DoRequest.get('/v1/cultures/locales');
    langs.value = res.data!['result'];
  }

  @override
  void onInit() {
    super.onInit();
    fetch();
  }
}

class ChangeLangPage extends StatelessWidget {
  const ChangeLangPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(ChangeLangController());
    return Scaffold(
      body: c.langs.isEmpty
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(21),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 16, bottom: 4),
                    child: SizedBox(
                      height: kToolbarHeight,
                      child: Row(
                        children: [
                          Expanded(
                            child: TextField(
                              decoration: InputDecoration(
                                hintText: t.searchLang,
                                enabledBorder: ThemeConstants.inputBorder.copyWith(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 11),
                          const CustomBackButton(
                            border: false,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => ListView.separated(
                        itemCount: c.langs.length,
                        separatorBuilder: (context, index) {
                          return const Divider(thickness: .3);
                        },
                        itemBuilder: (context, index) {
                          final lang = c.langs[index];
                          return ListTile(
                            title: Text(lang['name']),
                            onTap: () {
                              StorageService.locale = lang['code'];
                              Get.back();
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
