import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/horizontal_or_line.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/models/user.dart';
import 'package:shaheen_booking/pages/home/home.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

class SignUpController extends GetxController {
  final TextEditingController name = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController password = TextEditingController();

  void submit() async {
    try {
      var res = await DoRequest.post('/auth/register/user', data: {
        "name": name.text,
        "email": email.text,
        "phone": phone.text,
        "password": password.text,
      });
      if (res.data!['success'] == false) {
        return;
      }
      StorageService.token = res.data!['result']['token'];
      DoRequest.setToken(StorageService.token ?? '');

      Get.rawSnackbar(message: res.data!['message']);
      res = await DoRequest.get('/profile');
      StorageService.user = AuthService.to.user = UserModel.fromJson(res.data!['result']);
      Get.bottomSheet(const _Success());
    } on DioExp catch (e) {
      Get.rawSnackbar(message: e.response!.data!['message']);
    } on Exception {
      Get.rawSnackbar(message: 'Something went wrong.');
    }
  }
}

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(SignUpController());
    return Scaffold(
      appBar: AppBar(
        surfaceTintColor: Colors.transparent,
      ),
      body: Form(
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Text(
              t.auth.newAccount,
              style: const TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            Text(
              t.auth.pleaseCreateAccountFirst,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: ThemeColors.grayShade0,
              ),
            ),
            const SizedBox(height: 28),
            ListTile(
              dense: true,
              title: Text(
                t.auth.name,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              controller: c.name,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.name,
              decoration: InputDecoration(
                hintText: t.auth.putName,
                prefixIcon: SvgPicture.asset(
                  'assets/icons/person.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 8),
            ListTile(
              dense: true,
              title: Text(
                t.auth.email,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              controller: c.email,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: t.auth.putEmail,
                prefixIcon: SvgPicture.asset(
                  'assets/icons/mail.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 8),
            ListTile(
              dense: true,
              title: Text(
                t.auth.phoneNumber,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              controller: c.phone,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: t.auth.putPhoneNumber,
                prefixIcon: SvgPicture.asset(
                  'assets/icons/phone.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 8),
            ListTile(
              dense: true,
              title: Text(
                t.auth.pass,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            TextFormField(
              obscureText: true,
              controller: c.password,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                hintText: t.auth.putPass,
                suffixIcon: SvgPicture.asset(
                  'assets/icons/eye.svg',
                  fit: BoxFit.scaleDown,
                ),
                prefixIcon: SvgPicture.asset(
                  'assets/icons/lock.svg',
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            const SizedBox(height: 51),
            ElevatedButton(
              onPressed: c.submit,
              child: Text(t.auth.createAccount),
            ),
            HorizontalOrLine(
              height: 60,
              label: t.auth.signInWith,
            ),
            OutlinedButton(
              onPressed: () {},
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(t.auth.signWithGoogle),
                  const SizedBox(width: 11),
                  SvgPicture.asset(
                    'assets/icons/google.svg',
                    fit: BoxFit.scaleDown,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 6),
            OutlinedButton(
              onPressed: () {},
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(t.auth.signWithFacebook),
                  const SizedBox(width: 11),
                  SvgPicture.asset(
                    'assets/icons/facebook.svg',
                    fit: BoxFit.scaleDown,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Success extends StatelessWidget {
  const _Success();

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (context) {
        return Container(
          width: double.infinity,
          padding: const EdgeInsets.all(11),
          decoration: BoxDecoration(
            color: ThemeColors.light,
            borderRadius: BorderRadius.circular(25),
          ),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 8,
                width: 150,
                decoration: BoxDecoration(
                  color: const Color(0xFFD8DBE5),
                  borderRadius: BorderRadius.circular(11),
                ),
              ),
              const Spacer(),
              Container(
                decoration: BoxDecoration(
                  color: const Color(0xFFf2f2f5),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: SvgPicture.asset(
                  'assets/icons/success.svg',
                  width: 150,
                  height: 150,
                ),
              ),
              const SizedBox(height: 16),
              Text(
                t.auth.accountCreatedSuccess,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                t.auth.accountCreatedSuccessDetails,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: ThemeColors.grayShade0,
                ),
              ),
              const Spacer(),
              OutlinedButton(
                onPressed: () {
                  Get.offAll(() => const HomePage());
                },
                child: Text(t.wcontinue),
              ),
              const Spacer(),
            ],
          ),
        );
      },
    );
  }
}
