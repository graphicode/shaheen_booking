import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/pages/auth/login.dart';
import 'package:shaheen_booking/pages/auth/sign_up.dart';

class FirstOpen extends StatelessWidget {
  const FirstOpen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(11),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 80),
            Center(
              child: Image.asset(
                'assets/logo_colored.png',
                width: 170,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              t.firstOpen.hello,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 25,
              ),
            ),
            Text(
              t.firstOpen.welcomeMessage,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 18,
                color: Color(0xFF858585),
                fontWeight: FontWeight.w400,
              ),
            ),
            const Spacer(),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {
                      Get.to(() => const LoginPage());
                    },
                    child: Text(
                      t.auth.createAccount,
                    ),
                  ),
                ),
                const SizedBox(width: 11),
                Expanded(
                  child: OutlinedButton(
                    onPressed: () {
                      Get.to(() => const SignUpPage());
                    },
                    child: Text(
                      t.auth.login,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 30),
          ],
        ),
      ),
    );
  }
}
