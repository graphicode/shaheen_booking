import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/services/storage.dart';

class ChoosePlacePage extends StatefulWidget {
  const ChoosePlacePage({super.key});

  @override
  State<ChoosePlacePage> createState() => _ChoosePlacePageState();
}

class _ChoosePlacePageState extends State<ChoosePlacePage> {
  final data = <GeoPlace>[];
  final viewedData = <GeoPlace>[];
  String searchQ = 'c';
  Timer? searchTimer;

  void fetch() async {
    final res = await DoRequest.post('/v1/autosuggest/flights', data: {
      'locale': StorageService.locale,
      'market': StorageService.market,
      'currency': StorageService.currency.code,
      'searchTerm': searchQ,
    });
    data.clear();
    data.addAll((res.data!['result'] as List).map((e) => GeoPlace.fromJson(e)));
    viewedData.clear();
    viewedData.addAll(data);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    fetch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(21),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 4),
              child: SizedBox(
                height: kToolbarHeight,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        onChanged: (value) {
                          if (searchTimer != null) {
                            searchTimer!.cancel();
                          }

                          searchTimer = Timer(const Duration(milliseconds: 500), () {
                            searchQ = value;
                            fetch();
                          });
                        },
                        decoration: InputDecoration(
                          hintText: t.choosePlace,
                          enabledBorder: ThemeConstants.inputBorder.copyWith(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 11),
                    const CustomBackButton(
                      border: false,
                    ),
                  ],
                ),
              ),
            ),
            if (data.isEmpty)
              const Center(
                child: CircularProgressIndicator(),
              )
            else
              Expanded(
                child: ListView.separated(
                  itemCount: viewedData.length,
                  separatorBuilder: (context, index) {
                    return const Divider(
                      height: 0,
                      thickness: .3,
                    );
                  },
                  itemBuilder: (context, index) {
                    final GeoPlace d = viewedData[index];
                    return InkWell(
                      onTap: () {
                        Get.back(result: d);
                      },
                      child: Material(
                        color: Colors.transparent,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/icons/location.svg',
                                colorFilter: const ColorFilter.mode(
                                  ThemeColors.primaryShade2,
                                  BlendMode.srcIn,
                                ),
                              ),
                              const SizedBox(width: 7),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    d.country,
                                    style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Text(
                                    d.name,
                                    style: const TextStyle(
                                      fontSize: 11,
                                      color: ThemeColors.grayShade3,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class GeoPlace {
  final String entityId;
  final String iata;
  final String name;
  final String countryId;
  final String country;
  final String city;
  final String location;
  final String hierarchy;
  final String type;

  GeoPlace({
    required this.entityId,
    required this.iata,
    required this.name,
    required this.countryId,
    required this.country,
    required this.city,
    required this.location,
    required this.hierarchy,
    required this.type,
  });

  factory GeoPlace.fromJson(Map<String, dynamic> json) => GeoPlace(
        entityId: json["entityId"],
        iata: json["iata"],
        name: json["name"],
        countryId: json["countryId"],
        country: json["country"],
        city: json["city"],
        location: json["location"],
        hierarchy: json["hierarchy"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "entityId": entityId,
        "iata": iata,
        "name": name,
        "countryId": countryId,
        "country": country,
        "city": city,
        "location": location,
        "hierarchy": hierarchy,
        "type": type,
      };
}
