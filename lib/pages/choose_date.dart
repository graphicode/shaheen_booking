import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ChooseDatePage extends StatefulWidget {
  const ChooseDatePage({super.key});

  @override
  State<ChooseDatePage> createState() => _ChooseDatePageState();
}

class _ChooseDatePageState extends State<ChooseDatePage> {
  DateTime? selected;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.all(21),
        children: [
          const SizedBox(height: 18),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                t.chooseDate.date,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                ),
              ),
              const CustomBackButton(),
            ],
          ),
          const SizedBox(height: 21),
          SfCalendar(
            view: CalendarView.month,
            monthViewSettings: const MonthViewSettings(
              appointmentDisplayMode: MonthAppointmentDisplayMode.appointment,
            ),
            onTap: (calendarTapDetails) {
              setState(() {
                selected = calendarTapDetails.date;
              });
            },
          ),
          // const SizedBox(height: 31),
          // Text(
          //   t.chooseDate.time,
          //   textAlign: TextAlign.center,
          //   style: const TextStyle(
          //     fontSize: 16,
          //     fontWeight: FontWeight.w700,
          //   ),
          // ),
          // const SizedBox(height: 11),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceAround,
          //   children: [
          //     TextButton(
          //       child: const Text('02:00 PM'),
          //       onPressed: () {},
          //     ),
          //     TextButton(
          //       child: const Text('02:00 PM'),
          //       onPressed: () {},
          //     ),
          //   ],
          // ),
          const SizedBox(height: 11),
          ElevatedButton(
            onPressed: () {
              Get.back(result: selected);
            },
            child: Text(t.confirm),
          ),
        ],
      ),
    );
  }
}
