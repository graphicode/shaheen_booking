import 'package:flutter/material.dart';

class CustomTicketShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final pointOfStart = size.height - (size.height / 3.35);
    final path = Path()
      ..lineTo(0, pointOfStart - 20)
      ..quadraticBezierTo(size.width * 0.10, pointOfStart, 0, pointOfStart + 20)
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, pointOfStart + 20)
      ..quadraticBezierTo(size.width * 0.90, pointOfStart, size.width, pointOfStart - 20)
      ..lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class HalfDashedLinePainter extends CustomPainter {
  final double theCutOffPointFrac;
  final Color paintColor;
  const HalfDashedLinePainter({
    this.theCutOffPointFrac = 2.3,
    this.paintColor = const Color(0xFF62646c),
  });

  @override
  void paint(Canvas canvas, Size size) {
    final theCutOffPoint = size.width / theCutOffPointFrac;
    final centerOfHeight = size.height / 2;
    final paint = Paint()..color = paintColor;
    //
    canvas.drawLine(
      Offset(0, centerOfHeight),
      Offset(theCutOffPoint, centerOfHeight),
      paint..strokeWidth = 1,
    );
    // dashed
    var max = size.width - theCutOffPoint;
    var dashWidth = 5, dashSpace = 5;
    double startX = theCutOffPoint; // start from the half
    // final paint = Paint()..color = paintColor;
    while (max >= 0) {
      canvas.drawLine(Offset(startX, centerOfHeight), Offset(startX + dashWidth, centerOfHeight),
          paint..strokeWidth = 1);
      final space = (dashSpace + dashWidth);
      startX += space;
      max -= space.toInt();
    }
    //
    final end = Offset(size.width, centerOfHeight);
    final circlePaint = Paint()
      ..color = paintColor
      ..style = PaintingStyle.fill;
    const circleRadius = 4.0;
    canvas.drawCircle(end, circleRadius, circlePaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class DashedLinePainter extends CustomPainter {
  final Color paintColor;
  const DashedLinePainter({
    this.paintColor = const Color(0xFF62646c),
  });

  @override
  void paint(Canvas canvas, Size size) {
    final centerOfHeight = size.height / 2;
    final paint = Paint()..color = paintColor;
    // dashed
    var max = size.width;
    var dashWidth = 5, dashSpace = 5;
    double startX = 0;
    // final paint = Paint()..color = paintColor;
    while (max >= 0) {
      canvas.drawLine(Offset(startX, centerOfHeight), Offset(startX + dashWidth, centerOfHeight),
          paint..strokeWidth = 2);
      final space = (dashSpace + dashWidth);
      startX += space;
      max -= space.toInt();
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
