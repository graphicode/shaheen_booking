class TicketModel {
  final double? bestScore;
  final double? fastestScore;
  final double? cheapestScore;
  final List<PricingOption> pricingOptions;
  final List<Leg> legs;

  TicketModel({
    required this.bestScore,
    required this.fastestScore,
    required this.cheapestScore,
    required this.pricingOptions,
    required this.legs,
  });

  factory TicketModel.fromJson(Map<String, dynamic> json) => TicketModel(
        bestScore: json["bestScore"]?.toDouble(),
        fastestScore: json["fastestScore"]?.toDouble(),
        cheapestScore: json["cheapestScore"]?.toDouble(),
        pricingOptions:
            List<PricingOption>.from(json["pricingOptions"].map((x) => PricingOption.fromJson(x))),
        legs: List<Leg>.from(json["legs"].map((x) => Leg.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "bestScore": bestScore,
        "fastestScore": fastestScore,
        "cheapestScore": cheapestScore,
        "pricingOptions": List<dynamic>.from(pricingOptions.map((x) => x.toJson())),
        "legs": List<dynamic>.from(legs.map((x) => x.toJson())),
      };
}

class Leg {
  final String originPlaceName;
  final String originPlaceType;
  final String destinationPlaceName;
  final String destinationPlaceType;
  final DateTimeModel departureDateTime;
  final DateTimeModel arrivalDateTime;
  final int durationInMinutes;
  final int stopCount;

  Leg({
    required this.originPlaceName,
    required this.originPlaceType,
    required this.destinationPlaceName,
    required this.destinationPlaceType,
    required this.departureDateTime,
    required this.arrivalDateTime,
    required this.durationInMinutes,
    required this.stopCount,
  });

  factory Leg.fromJson(Map<String, dynamic> json) => Leg(
        originPlaceName: json["originPlaceName"],
        originPlaceType: json["originPlaceType"],
        destinationPlaceName: json["destinationPlaceName"],
        destinationPlaceType: json["destinationPlaceType"],
        departureDateTime: DateTimeModel.fromJson(json["departureDateTime"]),
        arrivalDateTime: DateTimeModel.fromJson(json["arrivalDateTime"]),
        durationInMinutes: json["durationInMinutes"],
        stopCount: json["stopCount"],
      );

  Map<String, dynamic> toJson() => {
        "originPlaceName": originPlaceName,
        "originPlaceType": originPlaceType,
        "destinationPlaceName": destinationPlaceName,
        "destinationPlaceType": destinationPlaceType,
        "departureDateTime": departureDateTime.toJson(),
        "arrivalDateTime": arrivalDateTime.toJson(),
        "durationInMinutes": durationInMinutes,
        "stopCount": stopCount,
      };
}

class DateTimeModel {
  final int year;
  final int month;
  final int day;
  final int hour;
  final int minute;
  final int second;

  DateTimeModel({
    required this.year,
    required this.month,
    required this.day,
    required this.hour,
    required this.minute,
    required this.second,
  });

  factory DateTimeModel.fromJson(Map<String, dynamic> json) => DateTimeModel(
        year: json["year"],
        month: json["month"],
        day: json["day"],
        hour: json["hour"],
        minute: json["minute"],
        second: json["second"],
      );

  Map<String, dynamic> toJson() => {
        "year": year,
        "month": month,
        "day": day,
        "hour": hour,
        "minute": minute,
        "second": second,
      };
}

class PricingOption {
  final String transferType;
  final Price price;
  final List<Flight> flights;

  PricingOption({
    required this.transferType,
    required this.price,
    required this.flights,
  });

  factory PricingOption.fromJson(Map<String, dynamic> json) => PricingOption(
        transferType: json["transferType"],
        price: Price.fromJson(json["price"]),
        flights: List<Flight>.from(json["flights"].map((x) => Flight.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "transferType": transferType,
        "price": price.toJson(),
        "flights": List<dynamic>.from(flights.map((x) => x.toJson())),
      };
}

class Flight {
  final String agentName;
  final String agentType;
  final String agentImage;
  final Price price;
  final String deepLink;

  Flight({
    required this.agentName,
    required this.agentType,
    required this.agentImage,
    required this.price,
    required this.deepLink,
  });

  factory Flight.fromJson(Map<String, dynamic> json) => Flight(
        agentName: json["agentName"],
        agentType: json["agentType"],
        agentImage: json["agentImage"],
        price: Price.fromJson(json["price"]),
        deepLink: json["deepLink"],
      );

  Map<String, dynamic> toJson() => {
        "agentName": agentName,
        "agentType": agentType,
        "agentImage": agentImage,
        "price": price.toJson(),
        "deepLink": deepLink,
      };
}

class Price {
  final String amount;
  final String unit;

  Price({
    required this.amount,
    required this.unit,
  });

  factory Price.fromJson(Map<String, dynamic> json) => Price(
        amount: json["amount"],
        unit: json["unit"],
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "unit": unit,
      };
}
