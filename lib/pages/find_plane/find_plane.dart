import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:in_app_webview/in_app_webview.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/datetime.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/services/storage.dart';

import 'filter.dart';
import 'models.dart';
import 'paints.dart';

class FindPlaneForm {
  final String market;
  final String locale;
  final String currency;
  final String originEntityId;
  final String destinationEntityId;
  final int adults;
  final String cabinClass;
  final DateTime departureDate;
  final List<int> childrenAges;
  final DateTime? returnDate;
  final String? originCode;
  final String? disCode;

  FindPlaneForm({
    required this.market,
    required this.locale,
    required this.currency,
    required this.originEntityId,
    required this.destinationEntityId,
    required this.adults,
    required this.cabinClass,
    required this.departureDate,
    required this.childrenAges,
    required this.returnDate,
    required this.originCode,
    required this.disCode,
  });
}

class FindPlaneController extends GetxController {
  final FindPlaneForm formData;
  FindPlaneController(this.formData);

  bool completed = false;
  final data = <TicketModel>[].obs;

  void fetch() async {
    final res = await DoRequest.post('/v1/search/create', data: {
      "market": StorageService.market,
      "locale": formData.locale,
      "currency": formData.currency,
      "originEntityId": formData.originEntityId,
      "destinationEntityId": formData.destinationEntityId,
      "adults": formData.adults,
      "cabinClass": formData.cabinClass,
      "departureDate": formData.departureDate.toIso8601String(),
      "childrenAges": formData.childrenAges,
      "returnDate": formData.returnDate?.toIso8601String()
    });
    if (res.data!['success'] == false) {
      Get.rawSnackbar(message: 'Server Error');
      return;
    }
    // print((res.data!['result']['itineraries'] as List).cast<Map>().fold<List<Map>>(
    //   [],
    //   (previousValue, element) => [...previousValue, ...element.values],
    // ).first);
    data.addAll((res.data!['result']['itineraries'] as List).cast<Map>().fold<List<Map>>(
      [],
      (previousValue, element) => [...previousValue, ...element.values],
    ).map((e) => TicketModel.fromJson(e.cast<String, dynamic>())));

    if (res.data!['result']['completed'] == false) {
      Timer.periodic(const Duration(seconds: 1), (timer) async {
        final newRes = await DoRequest.get('/v1/search/poll/${res.data!['result']['searchId']}');
        if (res.data!['success'] == false) {
          Get.rawSnackbar(message: 'Server Error');
          return;
        }
        data.addAll((newRes.data!['result']['itineraries'] as List).cast<Map>().fold<List<Map>>(
          [],
          (previousValue, element) => [...previousValue, ...element.values],
        ).map((e) => TicketModel.fromJson(e.cast<String, dynamic>())));
        if (res.data!['result']['completed'] == true) {
          timer.cancel();
          completed = true;
        }
      });
    }
  }

  @override
  void onInit() {
    fetch();
    super.onInit();
  }
}

class FindPlanePage extends StatelessWidget {
  const FindPlanePage(this.formData, {super.key});

  final FindPlaneForm formData;

  @override
  Widget build(BuildContext context) {
    final c = Get.put(FindPlaneController(formData));
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: SvgPicture.asset(
          'assets/icons/settings.svg',
        ),
        onPressed: () {
          Get.to(() => const FilterTicketsPage());
        },
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(
              top: 30,
              bottom: 11,
              left: 11,
              right: 11,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.vertical(
                bottom: Radius.circular(11),
              ),
              boxShadow: [
                BoxShadow(
                  blurRadius: 17,
                  spreadRadius: 7,
                  color: Colors.black.withOpacity(.08),
                ),
              ],
            ),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          formData.originCode!,
                          style: const TextStyle(
                            fontSize: 17,
                          ),
                        ),
                        const SizedBox(width: 4),
                        SvgPicture.asset(
                          'assets/icons/go.svg',
                        ),
                        const SizedBox(width: 7),
                        Text(
                          formData.disCode!,
                          style: const TextStyle(
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                    const CustomBackButton(),
                  ],
                ),
                // const SizedBox(height: 11),
                // Row(
                //   children: [
                //     // Expanded(
                //     //   child: TextField(
                //     //     decoration: InputDecoration(
                //     //       hintText: t.searchLang,
                //     //     ),
                //     //   ),
                //     // ),
                //     // const SizedBox(width: 11),
                //     SvgPicture.asset(
                //       'assets/icons/settings.svg',
                //     ),
                //   ],
                // ),
              ],
            ),
          ),
          Expanded(
            child: Obx(() => ListView.separated(
                  itemCount: c.data.length,
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 26),
                  separatorBuilder: (context, index) {
                    return const SizedBox(height: 16);
                  },
                  itemBuilder: (context, index) {
                    final ticket = c.data[index];
                    return GestureDetector(
                      onTap: () {
                        Get.to(
                          () => InAppWebView(
                            ticket.pricingOptions.first.flights.first.deepLink,
                            mDirection: TextDirection.ltr,
                            appBarBGColor: ThemeColors.primaryShade1,
                            bottomNavColor: ThemeColors.primaryShade2,
                            defaultTitle: true,
                            backIcon: const Icon(Icons.arrow_back_ios, color: Colors.white),
                            nextIcon: const Icon(Icons.arrow_forward_ios, color: Colors.white),
                            closeIcon: const Icon(Icons.close, color: Colors.white),
                            shareIcon: const Icon(Icons.share, color: Colors.white),
                            refreshIcon: const Icon(Icons.refresh, color: Colors.white),
                            actionWidget: const [],
                            actionsIconTheme: const IconThemeData(),
                            centerTitle: true,
                            titleTextStyle: const TextStyle(),
                            toolbarTextStyle: const TextStyle(),
                            toolbarHeight: 56,
                          ),
                        );
                      },
                      child: ClipPath(
                        clipper: CustomTicketShape(),
                        child: Container(
                          padding: const EdgeInsets.only(left: 15, right: 15, bottom: 13, top: 30),
                          decoration: BoxDecoration(
                            color: ThemeColors.grayShade7,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Column(
                            children: [
                              const Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Air',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 9,
                                    ),
                                  ),
                                  SizedBox(width: 4),
                                  // Image.network(
                                  //   'https://s3-alpha-sig.figma.com/img/af22/7229/bede40704a07c14397d71a82c4be0721?Expires=1710720000&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Fyy73glh2BWTW4s-d195mNLOGpU5wPvOUbt5oDejlL1sDrf30asQOJhXz1o4F1G0h5~QDW0JJfDyE0UlV~C91UMmXZgnkUz4ssyos6E9Bq2xz2UPS0qGK0Fm60XXAuiagaY9ZGcZ1gJwiesJ6MrwoHxEVyoj6PP-xz-gmGz2VF8ubwkGfUMwH6nAYHTQdhBO-3kDmuoCES21cOjS-1XV-Gd2jduLuhWrSwdbTHe00ZXCntlod6IQWg~PhGEH2THhm0S8b9JLwysg10AU1pWq9Q3w494si0w2-Gptxc5a3eXzgG6rPORbF9hTKgrV59KEIG502oPHAk90MN1L0vLV5Q__',
                                  //   width: 27,
                                  // ),
                                ],
                              ),
                              const SizedBox(height: 21),
                              Row(
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        ticket.legs.first.destinationPlaceName,
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w700,
                                          color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                      Text(
                                        formData.disCode!,
                                        style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          // color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                      Text(
                                        jsonDateToString(
                                          ticket.legs.first.arrivalDateTime.hour,
                                          ticket.legs.first.arrivalDateTime.minute,
                                        ),
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          // color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(width: 11),
                                  Expanded(
                                    child: Stack(
                                      children: [
                                        const CustomPaint(
                                          size: Size(double.infinity, 40),
                                          painter: HalfDashedLinePainter(
                                            theCutOffPointFrac: 2.3,
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.center,
                                          child: Container(
                                            width: 40,
                                            height: 40,
                                            alignment: Alignment.center,
                                            padding: const EdgeInsets.all(4),
                                            decoration: BoxDecoration(
                                              color: ThemeColors.primaryShade2,
                                              borderRadius: BorderRadius.circular(50),
                                            ),
                                            child: Transform.rotate(
                                              angle: -.4,
                                              child: Image.asset(
                                                'assets/icons/plane.png',
                                                width: 23,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 11),
                                  Column(
                                    children: [
                                      Text(
                                        ticket.legs.first.originPlaceName,
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w700,
                                          color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                      Text(
                                        formData.originCode!,
                                        style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          // color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                      Text(
                                        jsonDateToString(
                                          ticket.legs.first.departureDateTime.hour,
                                          ticket.legs.first.departureDateTime.minute,
                                        ),
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          // color: ThemeColors.grayShade1,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              if (ticket.legs.length == 2) const SizedBox(height: 16),
                              if (ticket.legs.length == 2)
                                Row(
                                  children: [
                                    Column(
                                      children: [
                                        Text(
                                          ticket.legs.last.originPlaceName,
                                          style: const TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w700,
                                            color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                        Text(
                                          formData.disCode!,
                                          style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600,
                                            // color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                        Text(
                                          jsonDateToString(
                                            ticket.legs.last.departureDateTime.hour,
                                            ticket.legs.last.departureDateTime.minute,
                                          ),
                                          style: const TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w600,
                                            // color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(width: 11),
                                    Expanded(
                                      child: Transform.flip(
                                        flipX: true,
                                        child: Stack(
                                          children: [
                                            const CustomPaint(
                                              size: Size(double.infinity, 40),
                                              painter: HalfDashedLinePainter(
                                                theCutOffPointFrac: 2.3,
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.center,
                                              child: Container(
                                                width: 40,
                                                height: 40,
                                                alignment: Alignment.center,
                                                padding: const EdgeInsets.all(4),
                                                decoration: BoxDecoration(
                                                  color: ThemeColors.primaryShade2,
                                                  borderRadius: BorderRadius.circular(50),
                                                ),
                                                child: Transform.rotate(
                                                  angle: -.4,
                                                  child: Image.asset(
                                                    'assets/icons/plane.png',
                                                    width: 23,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 11),
                                    Column(
                                      children: [
                                        Text(
                                          ticket.legs.last.destinationPlaceName,
                                          style: const TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w700,
                                            color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                        Text(
                                          formData.originCode!,
                                          style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600,
                                            // color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                        Text(
                                          jsonDateToString(
                                            ticket.legs.last.arrivalDateTime.hour,
                                            ticket.legs.last.arrivalDateTime.minute,
                                          ),
                                          style: const TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w600,
                                            // color: ThemeColors.grayShade1,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              const SizedBox(height: 30),
                              const CustomPaint(
                                size: Size(300, 5),
                                painter: DashedLinePainter(
                                  paintColor: ThemeColors.primaryShade5,
                                ),
                              ),
                              const SizedBox(height: 25),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/icons/business.svg',
                                      ),
                                      const SizedBox(width: 4),
                                      Text(
                                        () {
                                          switch (formData.cabinClass) {
                                            case 'economy':
                                              return t.home.travelPlans.economy;
                                            case 'premium_economy':
                                              return t.home.travelPlans.featured;
                                            case 'first':
                                              return t.home.travelPlans.firstClass;
                                            default:
                                              return t.home.travelPlans.business;
                                          }
                                        }(),
                                        style: const TextStyle(
                                          fontSize: 11,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    '${int.parse(ticket.pricingOptions.first.price.amount) / 1000} ${StorageService.currency.symbol}',
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      color: ThemeColors.primaryShade1,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                )),
          )
        ],
      ),
    );
  }
}
