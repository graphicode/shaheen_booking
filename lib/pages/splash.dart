import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rive/rive.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/models/user.dart';
import 'package:shaheen_booking/pages/first_open.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

import 'home/home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    fetch();
  }

  void fetch() async {
    try {
      DoRequest.setToken(StorageService.token ?? '');
      final res = await DoRequest.get('/profile');
      StorageService.user = AuthService.to.user = UserModel.fromJson(res.data!['result']);
      Get.offAll(() => AuthService.loggedIn ? const HomePage() : const FirstOpen());
    } on DioExp catch (e) {
      if (e.response?.statusCode == 401) {
        Get.offAll(() => const FirstOpen());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: ThemeColors.primaryShade1,
      child: Stack(
        children: [
          Positioned(
            top: 100,
            left: 27,
            child: Image.asset(
              'assets/logo.png',
              width: 150,
            ),
          ),
          const Positioned(
            left: 0,
            top: 130,
            width: 400,
            height: 900,
            child: RiveAnimation.asset(
              'assets/new_file.riv',
              fit: BoxFit.fitHeight,
            ),
          ),
        ],
      ),
    );
  }
}
