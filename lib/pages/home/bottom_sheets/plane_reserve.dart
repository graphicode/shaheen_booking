import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/pages/choose_date.dart';
import 'package:shaheen_booking/pages/choose_place.dart';
import 'package:shaheen_booking/pages/find_plane/find_plane.dart';
import 'package:shaheen_booking/services/storage.dart';

class _PlaneReserveConroller extends GetxController {
  final justGo = false.obs;
  final adultsCount = 1.obs;
  final kidsAges = <TextEditingController>[].obs;
  final origin = Rxn<GeoPlace>();
  final destination = Rxn<GeoPlace>();
  final cabinClass = 'economy'.obs;

  final dateFrom = Rxn<DateTime>();
  final dateTo = Rxn<DateTime>();

  submitAndSearch() async {
    final form = FindPlaneForm(
      market: 'EG',
      originCode: origin.value!.iata,
      disCode: destination.value!.iata,
      locale: StorageService.locale,
      currency: StorageService.currency.code,
      originEntityId: origin.value!.entityId,
      destinationEntityId: destination.value!.entityId,
      adults: adultsCount.value,
      cabinClass: cabinClass.value,
      departureDate: dateFrom.value!,
      returnDate: dateTo.value,
      childrenAges: kidsAges.map<int>((e) => int.parse(e.text)).toList(),
    );
    Get.to(() => FindPlanePage(form));
  }
}

class PlaneReserveBottomSheet extends StatelessWidget {
  const PlaneReserveBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(_PlaneReserveConroller());
    return DraggableScrollableSheet(
      initialChildSize: 0.5,
      minChildSize: 0.2,
      expand: false,
      maxChildSize: 0.8,
      builder: (context, sc) {
        return Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(25),
            ),
          ),
          child: ListView(
            controller: sc,
            padding: const EdgeInsets.all(14),
            children: [
              UnconstrainedBox(
                child: Container(
                  height: 8,
                  width: 150,
                  decoration: BoxDecoration(
                    color: const Color(0xFFD8DBE5),
                    borderRadius: BorderRadius.circular(11),
                  ),
                ),
              ),
              const SizedBox(height: 14),
              Obx(() => Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            c.justGo.value = false;
                          },
                          child: Column(
                            children: [
                              Text(
                                t.home.goAndBack,
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  color: c.justGo.value ? null : const Color(0xFF4A2BED),
                                ),
                              ),
                              const SizedBox(height: 3),
                              if (c.justGo.value)
                                const Divider(
                                  thickness: 2,
                                  color: Color(0xFFe6e6e6),
                                )
                              else
                                const Divider(
                                  thickness: 2,
                                  color: Color(0xFF4A2BED),
                                ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            c.justGo.value = true;
                          },
                          child: Column(
                            children: [
                              Text(
                                t.home.justGo,
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  color: c.justGo.value ? const Color(0xFF4A2BED) : null,
                                ),
                              ),
                              const SizedBox(height: 3),
                              if (c.justGo.value)
                                const Divider(
                                  thickness: 2,
                                  color: Color(0xFF4A2BED),
                                )
                              else
                                const Divider(
                                  thickness: 2,
                                  color: Color(0xFFe6e6e6),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
              const SizedBox(height: 11),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 11),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          t.home.from,
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w700,
                            color: ThemeColors.grayShade3,
                          ),
                        ),
                        const SizedBox(height: 7),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            final GeoPlace? res = await Get.to(() => const ChoosePlacePage());
                            if (res != null) c.origin.value = res;
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                color: const Color(0xFFe6e6e6),
                              ),
                            ),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/icons/location.svg',
                                  colorFilter: const ColorFilter.mode(
                                    ThemeColors.primaryShade2,
                                    BlendMode.srcIn,
                                  ),
                                ),
                                const SizedBox(width: 7),
                                Obx(() => Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          c.origin.value?.country ?? t.home.choose,
                                          style: const TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Text(
                                          c.origin.value?.name ?? t.home.choose,
                                          style: const TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 21),
                        Text(
                          t.home.to,
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w700,
                            color: ThemeColors.grayShade3,
                          ),
                        ),
                        const SizedBox(height: 7),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            final GeoPlace? res = await Get.to(() => const ChoosePlacePage());
                            if (res != null) c.destination.value = res;
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                color: const Color(0xFFe6e6e6),
                              ),
                            ),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/icons/location.svg',
                                  colorFilter: const ColorFilter.mode(
                                    ThemeColors.primaryShade2,
                                    BlendMode.srcIn,
                                  ),
                                ),
                                const SizedBox(width: 7),
                                Obx(() => Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          c.destination.value?.country ?? t.home.choose,
                                          style: const TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Text(
                                          c.destination.value?.name ?? t.home.choose,
                                          style: const TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 25,
                      left: 0,
                      bottom: 0,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: SvgPicture.asset(
                          'assets/icons/from_to.svg',
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 11),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 11),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                    color: const Color(0xFFe6e6e6),
                  ),
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final DateTime? res = await Get.to(() => const ChooseDatePage());
                        if (res != null) c.dateFrom.value = res;
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 18,
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/calendar.svg',
                            ),
                            const SizedBox(width: 7),
                            Obx(() => Text(
                                  c.dateFrom.value != null
                                      ? Jiffy.parseFromDateTime(c.dateFrom.value!).MMMMEEEEd
                                      : t.home.timeToArrive,
                                  style: const TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                    color: ThemeColors.grayShade3,
                                  ),
                                )),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final DateTime? res = await Get.to(() => const ChooseDatePage());
                        if (res != null) c.dateTo.value = res;
                      },
                      child: Obx(() => Column(
                            children: [
                              if (c.justGo.value == false)
                                const Divider(
                                  height: 0,
                                  color: Color(0xFFe6e6e6),
                                ),
                              if (c.justGo.value == false)
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 16,
                                    horizontal: 18,
                                  ),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/icons/calendar.svg',
                                      ),
                                      const SizedBox(width: 7),
                                      Obx(() => Text(
                                            c.dateTo.value != null
                                                ? Jiffy.parseFromDateTime(c.dateTo.value!).MMMMEEEEd
                                                : t.home.timeToBack,
                                            style: const TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.w700,
                                              color: ThemeColors.grayShade3,
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                            ],
                          )),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 11),
              DropdownButtonFormField<String>(
                value: c.cabinClass.value,
                borderRadius: BorderRadius.circular(10),
                onChanged: (value) {
                  c.cabinClass.value = value ?? 'economy';
                },
                items: [
                  DropdownMenuItem(
                    value: 'unspecified',
                    child: Text(t.home.travelPlans.unspecified),
                  ),
                  DropdownMenuItem(
                    value: 'economy',
                    child: Text(t.home.travelPlans.economy),
                  ),
                  DropdownMenuItem(
                    value: 'premium_economy',
                    child: Text(t.home.travelPlans.featured),
                  ),
                  DropdownMenuItem(
                    value: 'business',
                    child: Text(t.home.travelPlans.business),
                  ),
                  DropdownMenuItem(
                    value: 'first',
                    child: Text(t.home.travelPlans.firstClass),
                  ),
                ],
              ),
              const SizedBox(height: 13),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 11),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      t.home.passengers,
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        color: ThemeColors.grayShade3,
                      ),
                    ),
                    const SizedBox(height: 9),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Obx(() => Text(
                                  c.adultsCount.value.toString(),
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            const SizedBox(width: 11),
                            Text(
                              t.home.adults,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                c.adultsCount.value++;
                              },
                              icon: const Icon(Icons.keyboard_arrow_up_rounded),
                            ),
                            IconButton(
                              onPressed: () {
                                c.adultsCount.value--;
                              },
                              icon: const Icon(Icons.keyboard_arrow_down_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 11),
                    Text(
                      t.home.kids,
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        color: ThemeColors.grayShade3,
                      ),
                    ),
                    const SizedBox(height: 9),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Obx(() => Text(
                                  c.kidsAges.length.toString(),
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            const SizedBox(width: 11),
                            Text(
                              t.home.kids,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                c.kidsAges.add(TextEditingController());
                              },
                              icon: const Icon(Icons.keyboard_arrow_up_rounded),
                            ),
                            IconButton(
                              onPressed: () {
                                c.kidsAges.removeLast();
                              },
                              icon: const Icon(Icons.keyboard_arrow_down_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 13),
              Obx(() => Column(
                    children: [
                      for (final (i, tc) in c.kidsAges.indexed)
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextField(
                            controller: tc,
                            decoration: InputDecoration(hintText: 'عمر الطفل ${i + 1}'),
                          ),
                        ),
                    ],
                  )),
              const SizedBox(height: 21),
              ElevatedButton(
                onPressed: c.submitAndSearch,
                child: Text(t.search),
              ),
            ],
          ),
        );
      },
    );
  }
}
