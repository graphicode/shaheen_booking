import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:in_app_webview/in_app_webview.dart';
import 'package:jiffy/jiffy.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/pages/choose_date.dart';
import 'package:shaheen_booking/pages/choose_place.dart';

class _HotelReserveConroller extends GetxController {
  final justGo = false.obs;
  final adultsCount = 1.obs;
  final kidsCount = 0.obs;
  final roomCount = 1.obs;
  final destination = Rxn<GeoPlace>();
  final kidsAges = <TextEditingController>[].obs;

  final dateFrom = Rxn<DateTime>();
  final dateTo = Rxn<DateTime>();

  void fetch() async {
    final result = await DoRequest.post('/v1/search/hotels', data: {
      'adults': adultsCount.value,
      'rooms': roomCount.value,
      'destinationEntityId': destination.value!.entityId,
      'checkin': dateFrom.value!.toIso8601String(),
      'checkout': dateTo.value!.toIso8601String(),
    });
    if (result.data!['success'] == true) {
      Get.to(
        () => InAppWebView(
          result.data!['result']['link'],
          mDirection: TextDirection.ltr,
          appBarBGColor: ThemeColors.primaryShade1,
          bottomNavColor: ThemeColors.primaryShade2,
          defaultTitle: true,
          backIcon: const Icon(Icons.arrow_back_ios, color: Colors.white),
          nextIcon: const Icon(Icons.arrow_forward_ios, color: Colors.white),
          closeIcon: const Icon(Icons.close, color: Colors.white),
          shareIcon: const Icon(Icons.share, color: Colors.white),
          refreshIcon: const Icon(Icons.refresh, color: Colors.white),
          actionWidget: const [],
          actionsIconTheme: const IconThemeData(),
          centerTitle: true,
          titleTextStyle: const TextStyle(),
          toolbarTextStyle: const TextStyle(),
          toolbarHeight: 56,
        ),
      );
    }
  }
}

class HotelReserveBottomSheet extends StatelessWidget {
  const HotelReserveBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(_HotelReserveConroller());
    return DraggableScrollableSheet(
      initialChildSize: 0.5,
      minChildSize: 0.2,
      expand: false,
      maxChildSize: 0.8,
      builder: (context, sc) {
        return Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(25),
            ),
          ),
          child: ListView(
            controller: sc,
            padding: const EdgeInsets.all(14),
            children: [
              UnconstrainedBox(
                child: Container(
                  height: 8,
                  width: 150,
                  decoration: BoxDecoration(
                    color: const Color(0xFFD8DBE5),
                    borderRadius: BorderRadius.circular(11),
                  ),
                ),
              ),
              const SizedBox(height: 14),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 11),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      t.home.from,
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        color: ThemeColors.grayShade3,
                      ),
                    ),
                    const SizedBox(height: 7),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final GeoPlace? res = await Get.to(() => const ChoosePlacePage());
                        if (res != null) c.destination.value = res;
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(
                            color: const Color(0xFFe6e6e6),
                          ),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/location.svg',
                              colorFilter: const ColorFilter.mode(
                                ThemeColors.primaryShade2,
                                BlendMode.srcIn,
                              ),
                            ),
                            const SizedBox(width: 7),
                            Obx(() => Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      c.destination.value?.country ?? t.home.choose,
                                      style: const TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Text(
                                      c.destination.value?.name ?? t.home.choose,
                                      style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 11),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 11),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                    color: const Color(0xFFe6e6e6),
                  ),
                ),
                child: Column(
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final DateTime? res = await Get.to(() => const ChooseDatePage());
                        if (res != null) c.dateFrom.value = res;
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: 18,
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              'assets/icons/calendar.svg',
                            ),
                            const SizedBox(width: 7),
                            Obx(() => Text(
                                  c.dateFrom.value != null
                                      ? Jiffy.parseFromDateTime(c.dateFrom.value!).MMMMEEEEd
                                      : t.home.timeToArrive,
                                  style: const TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                    color: ThemeColors.grayShade3,
                                  ),
                                )),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        final DateTime? res = await Get.to(() => const ChooseDatePage());
                        if (res != null) c.dateTo.value = res;
                      },
                      child: Obx(() => Column(
                            children: [
                              const Divider(
                                height: 0,
                                color: Color(0xFFe6e6e6),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 16,
                                  horizontal: 18,
                                ),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      'assets/icons/calendar.svg',
                                    ),
                                    const SizedBox(width: 7),
                                    Obx(() => Text(
                                          c.dateTo.value != null
                                              ? Jiffy.parseFromDateTime(c.dateTo.value!).MMMMEEEEd
                                              : t.home.timeToBack,
                                          style: const TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w700,
                                            color: ThemeColors.grayShade3,
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 13),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 11),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      t.home.hotelGuests,
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                        color: ThemeColors.grayShade3,
                      ),
                    ),
                    const SizedBox(height: 9),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Obx(() => Text(
                                  c.roomCount.value.toString(),
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            const SizedBox(width: 11),
                            Text(
                              t.home.room,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                c.roomCount.value++;
                              },
                              icon: const Icon(Icons.keyboard_arrow_up_rounded),
                            ),
                            IconButton(
                              onPressed: () {
                                c.roomCount.value--;
                              },
                              icon: const Icon(Icons.keyboard_arrow_down_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 11),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Obx(() => Text(
                                  c.adultsCount.value.toString(),
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            const SizedBox(width: 11),
                            Text(
                              t.home.adults,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                c.adultsCount.value++;
                              },
                              icon: const Icon(Icons.keyboard_arrow_up_rounded),
                            ),
                            IconButton(
                              onPressed: () {
                                c.adultsCount.value--;
                              },
                              icon: const Icon(Icons.keyboard_arrow_down_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 11),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Obx(() => Text(
                                  c.kidsCount.value.toString(),
                                  style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                            const SizedBox(width: 11),
                            Text(
                              t.home.kids,
                              style: const TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                c.kidsCount.value++;
                                c.kidsAges.add(TextEditingController());
                              },
                              icon: const Icon(Icons.keyboard_arrow_up_rounded),
                            ),
                            IconButton(
                              onPressed: () {
                                c.kidsCount.value--;
                                c.kidsAges.removeLast();
                              },
                              icon: const Icon(Icons.keyboard_arrow_down_rounded),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 13),
              Obx(() => Column(
                    children: [
                      for (final tc in c.kidsAges)
                        TextField(
                          controller: tc,
                          decoration: const InputDecoration(hintText: 'عمر الطفل'),
                        ),
                    ],
                  )),
              const SizedBox(height: 21),
              ElevatedButton(
                onPressed: c.fetch,
                child: Text(t.search),
              )
            ],
          ),
        );
      },
    );
  }
}
