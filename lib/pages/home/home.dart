import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/core/constants/theme.dart';

import 'main.dart';
import 'profile.dart';

class HomeController extends GetxController {
  final currentIndex = 0.obs;
  final PageController pageController = PageController(initialPage: 0, keepPage: true);
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final c = Get.put(HomeController());
    return Scaffold(
      bottomNavigationBar: Obx(() => Container(
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1.5,
                  color: Color(0xFFF3F3F3),
                ),
              ),
            ),
            child: BottomNavigationBar(
              elevation: 0,
              currentIndex: c.currentIndex.value,
              backgroundColor: theme.scaffoldBackgroundColor,
              selectedItemColor: ThemeColors.primaryShade2,
              onTap: (value) {
                c.currentIndex.value = value;
                c.pageController.animateToPage(
                  value,
                  duration: kThemeAnimationDuration,
                  curve: Curves.easeIn,
                );
              },
              items: [
                BottomNavigationBarItem(
                  label: 'Discover',
                  icon: ClipRRect(
                    borderRadius: BorderRadius.circular(11),
                    child: SvgPicture.asset(
                      'assets/icons/discover.svg',
                      colorFilter: c.currentIndex.value != 0
                          ? null
                          : const ColorFilter.mode(
                              ThemeColors.primaryShade2,
                              BlendMode.color,
                            ),
                    ),
                  ),
                ),
                // BottomNavigationBarItem(
                //   label: 'Search',
                //   icon: SvgPicture.asset(
                //     'assets/icons/search.svg',
                //     colorFilter: c.currentIndex.value != 1
                //         ? null
                //         : const ColorFilter.mode(
                //             ThemeColors.primaryShade2,
                //             BlendMode.srcIn,
                //           ),
                //   ),
                // ),
                BottomNavigationBarItem(
                  label: 'Profile',
                  icon: SvgPicture.asset(
                    'assets/icons/person.svg',
                    colorFilter: c.currentIndex.value != 2
                        ? null
                        : const ColorFilter.mode(
                            ThemeColors.primaryShade2,
                            BlendMode.srcIn,
                          ),
                  ),
                ),
              ],
            ),
          )),
      body: PageView(
        controller: c.pageController,
        physics: const NeverScrollableScrollPhysics(),
        children: const [
          MainSubPage(),
          // Material(),
          ProfileSubPage(),
        ],
      ),
    );
  }
}
