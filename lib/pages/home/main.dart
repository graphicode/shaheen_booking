import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:in_app_webview/in_app_webview.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

import 'bottom_sheets/hotel_reserve.dart';
import 'bottom_sheets/plane_reserve.dart';

class RecommendedItem {
  final String agentName;
  final String agentType;
  final String agentImage;
  final String deepLink;

  RecommendedItem({
    required this.agentName,
    required this.agentType,
    required this.agentImage,
    required this.deepLink,
  });

  factory RecommendedItem.fromJson(Map<String, dynamic> json) => RecommendedItem(
        agentName: json["agentName"],
        agentType: json["agentType"],
        agentImage: json["agentImage"],
        deepLink: json["deepLink"],
      );

  Map<String, dynamic> toJson() => {
        "agentName": agentName,
        "agentType": agentType,
        "agentImage": agentImage,
        "deepLink": deepLink,
      };
}

class MainSubController extends GetxController {
  final reservePlane = false.obs;

  final recommended = <RecommendedItem>[].obs;

  void fetch() async {
    final result = await DoRequest.get('/v1/autosuggest/recomends');
    if (result.data!['success'] == true) {
      recommended.value =
          (result.data!['result'] as List).map((e) => RecommendedItem.fromJson(e)).toList();
    }
  }

  @override
  void onInit() {
    fetch();
    super.onInit();
  }
}

class MainSubPage extends StatelessWidget {
  const MainSubPage({super.key});

  @override
  Widget build(BuildContext context) {
    final c = Get.put(MainSubController());
    return Column(
      children: [
        Container(
          height: 320,
          width: double.infinity,
          clipBehavior: Clip.none,
          padding: const EdgeInsets.all(11),
          decoration: const BoxDecoration(
            color: ThemeColors.primaryShade2,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40),
            ),
          ),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                left: -50,
                bottom: -50,
                right: -50,
                top: -50,
                child: Transform.rotate(
                  angle: -.5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 70,
                        width: Get.height * 2,
                        color: Colors.white.withOpacity(.1),
                      ),
                      Divider(
                        color: Colors.white.withOpacity(.15),
                        height: 0,
                      ),
                      Container(
                        height: 60,
                        width: Get.height * 2,
                        color: Colors.white.withOpacity(.07),
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                children: [
                  const SizedBox(height: 30),
                  Center(
                    child: Image.asset(
                      'assets/logo.png',
                      width: 120,
                    ),
                  ),
                  const SizedBox(height: 21),
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundImage: NetworkImage(AuthService.currentUser.img!),
                      ),
                      const SizedBox(width: 11),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            t.home.hellowMr(name: AuthService.currentUser.name),
                            style: const TextStyle(
                              fontSize: 16,
                              color: ThemeColors.light,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            AuthService.currentUser.email,
                            style: const TextStyle(
                              fontSize: 11,
                              color: ThemeColors.light,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                      const Spacer(),
                      Row(
                        children: [
                          Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Center(child: Text(StorageService.currency.code)),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
              Obx(() => Positioned(
                    left: 0,
                    right: 0,
                    bottom: -70,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                c.reservePlane.value = false;
                              },
                              child: Opacity(
                                opacity: c.reservePlane.value ? .4 : 1,
                                child: Container(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 15, vertical: 6.3),
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(50),
                                    border: Border.all(width: .6, color: Colors.white),
                                  ),
                                  child: Text(
                                    t.home.hotelReserve,
                                    style: const TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 7),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                c.reservePlane.value = true;
                              },
                              child: Opacity(
                                opacity: c.reservePlane.value ? 1 : .4,
                                child: Container(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 15, vertical: 6.3),
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(50),
                                    border: Border.all(width: .6, color: Colors.white),
                                  ),
                                  child: Text(
                                    t.home.planeReserve,
                                    style: const TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 11),
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            if (c.reservePlane.value) {
                              showModalBottomSheet(
                                context: context,
                                enableDrag: true,
                                isDismissible: true,
                                isScrollControlled: true,
                                builder: (_) => const PlaneReserveBottomSheet(),
                              );
                            } else {
                              showModalBottomSheet(
                                context: context,
                                enableDrag: true,
                                isDismissible: true,
                                isScrollControlled: true,
                                builder: (_) => const HotelReserveBottomSheet(),
                              );
                            }
                          },
                          child: Container(
                            width: double.infinity,
                            padding: const EdgeInsets.only(
                              top: 18,
                              bottom: 35,
                              left: 21,
                              right: 22,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: [
                                BoxShadow(
                                  offset: const Offset(0, 20),
                                  blurRadius: 30,
                                  spreadRadius: 11,
                                  color: Colors.black.withOpacity(.07),
                                )
                              ],
                            ),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Transform.rotate(
                                      angle: c.reservePlane.value ? -.4 : 0,
                                      child: Image.asset(
                                        c.reservePlane.value
                                            ? 'assets/icons/plane.png'
                                            : 'assets/icons/hotel.png',
                                        height: 22,
                                        color: const Color(0xFFd7d7d7),
                                      ),
                                    ),
                                    const SizedBox(width: 4),
                                    Text(
                                      c.reservePlane.value
                                          ? t.home.enjoyYourTravelExperiance
                                          : t.home.whereToStay,
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 20),
                                Material(
                                  color: Colors.transparent,
                                  child: SizedBox(
                                    height: 35,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          c.reservePlane.value
                                              ? t.home.reserveNow
                                              : t.home.findYourWay,
                                          style: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SvgPicture.asset(
                                          'assets/icons/go.svg',
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: .8,
                                  color: const Color(0xFF6950F0).withOpacity(.51),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
        Expanded(
          child: ListView(
            children: [
              const SizedBox(height: 100),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  children: [
                    Text(
                      t.home.famousPlaces,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 150,
                child: Obx(() => ListView.separated(
                      itemCount: c.recommended.length,
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.only(right: 27, left: 27),
                      separatorBuilder: (context, index) {
                        return const SizedBox(width: 16);
                      },
                      itemBuilder: (context, index) {
                        final r = c.recommended[index];
                        return GestureDetector(
                          onTap: () {
                            Get.to(
                              () => InAppWebView(
                                r.deepLink,
                                mDirection: TextDirection.ltr,
                                appBarBGColor: ThemeColors.primaryShade1,
                                bottomNavColor: ThemeColors.primaryShade2,
                                defaultTitle: true,
                                backIcon: const Icon(Icons.arrow_back_ios, color: Colors.white),
                                nextIcon: const Icon(Icons.arrow_forward_ios, color: Colors.white),
                                closeIcon: const Icon(Icons.close, color: Colors.white),
                                shareIcon: const Icon(Icons.share, color: Colors.white),
                                refreshIcon: const Icon(Icons.refresh, color: Colors.white),
                                actionWidget: const [],
                                actionsIconTheme: const IconThemeData(),
                                centerTitle: true,
                                titleTextStyle: const TextStyle(),
                                toolbarTextStyle: const TextStyle(),
                                toolbarHeight: 56,
                              ),
                            );
                          },
                          child: Container(
                            width: 200,
                            height: 150,
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(r.agentImage),
                              ),
                            ),
                            child: Stack(
                              children: [
                                Container(
                                  width: double.infinity,
                                  height: double.infinity,
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                      end: Alignment.topCenter,
                                      begin: Alignment.bottomCenter,
                                      colors: [
                                        Colors.black38,
                                        Colors.transparent,
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 14,
                                  right: 14,
                                  bottom: 21,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            r.agentName,
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          // const SizedBox(height: 11),
                                          // Row(
                                          //   mainAxisAlignment: MainAxisAlignment.start,
                                          //   crossAColor.fromARGB(255, 68, 62, 62): CrossAxisAlignment.center,
                                          //   children: [
                                          //     for (int i = 0; i < 5; i++)
                                          //       Icon(
                                          //         Icons.star,
                                          //         size: 14,
                                          //         color: Colors.yellow.shade700,
                                          //       ),
                                          //     const SizedBox(width: 7),
                                          //     const Text(
                                          //       '3.4',
                                          //       style: TextStyle(
                                          //         color: Colors.white,
                                          //       ),
                                          //     ),
                                          //   ],
                                          // ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )),
              )
            ],
          ),
        ),
      ],
    );
  }
}
