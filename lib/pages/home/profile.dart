import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:shaheen_booking/components/back_button.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/core/helpers/dio_helper.dart';
import 'package:shaheen_booking/core/i18n/strings.g.dart';
import 'package:shaheen_booking/pages/auth/edit_profile.dart';
import 'package:shaheen_booking/pages/auth/settings/change_lang.dart';
import 'package:shaheen_booking/pages/auth/settings/change_market.dart';
import 'package:shaheen_booking/pages/auth/settings/currency.dart';
import 'package:shaheen_booking/pages/first_open.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

class ProfileSubPage extends StatelessWidget {
  const ProfileSubPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 14,
      ),
      children: [
        const SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              t.profile.title,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
              ),
            ),
            const CustomBackButton(),
          ],
        ),
        const SizedBox(height: 21),
        Row(
          children: [
            CircleAvatar(
              radius: 35,
              backgroundImage: NetworkImage(AuthService.currentUser.img!),
            ),
            const SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AuthService.currentUser.name,
                  style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  AuthService.currentUser.email,
                  style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: ThemeColors.grayShade0,
                  ),
                ),
              ],
            ),
            const Spacer(),
            TextButton(
              onPressed: () {
                Get.to(() => const EditProfilePage());
              },
              child: Row(
                children: [
                  Text(t.profile.edit),
                  const SizedBox(width: 4),
                  const Icon(
                    Icons.arrow_forward_ios,
                    size: 12,
                  ),
                ],
              ),
            ),
          ],
        ),
        const Divider(
          height: 30,
          thickness: .3,
        ),
        ListTile(
          onTap: () {
            Get.to(() => const ChangeMarketPage());
          },
          leading: SvgPicture.asset(
            'assets/icons/location.svg',
          ),
          trailing: const Icon(
            Icons.arrow_forward_ios,
            size: 16,
            color: ThemeColors.primaryShade2,
          ),
          title: Text(
            t.profile.mainLocation,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w700,
            ),
          ),
          subtitle: Text(
            t.profile.mainLocationDescription,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        // ListTile(
        //   leading: SvgPicture.asset(
        //     'assets/icons/person.svg',
        //   ),
        //   trailing: const Icon(
        //     Icons.arrow_forward_ios,
        //     size: 16,
        //     color: ThemeColors.primaryShade2,
        //   ),
        //   title: Text(
        //     t.profile.paymentMethod,
        //     style: const TextStyle(
        //       fontSize: 12,
        //       fontWeight: FontWeight.w700,
        //     ),
        //   ),
        //   subtitle: Text(
        //     t.profile.paymentMethodDescription,
        //     style: const TextStyle(
        //       fontSize: 12,
        //       fontWeight: FontWeight.w400,
        //     ),
        //   ),
        // ),
        ListTile(
          onTap: () {
            Get.to(() => const CurrencyPage());
          },
          leading: SvgPicture.asset(
            'assets/icons/currency.svg',
          ),
          trailing: const Icon(
            Icons.arrow_forward_ios,
            size: 16,
            color: ThemeColors.primaryShade2,
          ),
          title: Text(
            t.profile.currency,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w700,
            ),
          ),
          subtitle: Text(
            t.profile.currencyDescription,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        ListTile(
          onTap: () {
            Get.to(() => const ChangeLangPage());
          },
          leading: SvgPicture.asset(
            'assets/icons/lang.svg',
          ),
          trailing: const Icon(
            Icons.arrow_forward_ios,
            size: 16,
            color: ThemeColors.primaryShade2,
          ),
          title: Text(
            t.profile.lang,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w700,
            ),
          ),
          subtitle: Text(
            t.profile.langDescription,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        // ListTile(
        //   leading: SvgPicture.asset(
        //     'assets/icons/history.svg',
        //   ),
        //   trailing: const Icon(
        //     Icons.arrow_forward_ios,
        //     size: 16,
        //     color: ThemeColors.primaryShade2,
        //   ),
        //   title: Text(
        //     t.profile.history,
        //     style: const TextStyle(
        //       fontSize: 12,
        //       fontWeight: FontWeight.w700,
        //     ),
        //   ),
        //   subtitle: Text(
        //     t.profile.historyDescription,
        //     style: const TextStyle(
        //       fontSize: 12,
        //       fontWeight: FontWeight.w400,
        //     ),
        //   ),
        // ),
        ListTile(
          onTap: () {
            StorageService.token = null;
            DoRequest.deleteToken();
            Get.offAll(() => const FirstOpen());
          },
          leading: SvgPicture.asset(
            'assets/icons/logout.svg',
            colorFilter: ColorFilter.mode(
              Colors.red.shade300,
              BlendMode.srcIn,
            ),
          ),
          title: Text(
            t.profile.logout,
            style: TextStyle(
              fontSize: 12,
              color: Colors.red.shade300,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ],
    );
  }
}
