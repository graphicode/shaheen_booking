import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shaheen_booking/core/constants/theme.dart';
import 'package:shaheen_booking/services/auth.dart';
import 'package:shaheen_booking/services/storage.dart';

import 'core/i18n/strings.g.dart';
import 'pages/splash.dart';
import 'services/settings.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Get.putAsync(() => StorageService().init());
  await Get.putAsync(() => SettingsService().init());
  await Get.putAsync(() => AuthService().init());
  runApp(TranslationProvider(child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Shaheen Booking',
      home: const SplashScreen(),
      debugShowCheckedModeBanner: false,
      locale: TranslationProvider.of(context).flutterLocale,
      supportedLocales: AppLocaleUtils.supportedLocales,
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
      theme: ThemeData(
        useMaterial3: true,
        scaffoldBackgroundColor: ThemeColors.light,
        colorScheme: ColorScheme.fromSeed(
          seedColor: ThemeColors.primary,
          onBackground: ThemeColors.textColor,
        ),
        textTheme: GoogleFonts.almaraiTextTheme(),
        fontFamily: GoogleFonts.almarai().fontFamily,
        appBarTheme: const AppBarTheme(
          backgroundColor: ThemeColors.light,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarBrightness: Brightness.dark,
            statusBarIconBrightness: Brightness.dark,
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all(ThemeColors.primaryShade1),
            backgroundColor: MaterialStateProperty.all(ThemeColors.light),
            minimumSize: MaterialStateProperty.all(const Size(double.infinity, 56)),
            textStyle: MaterialStateProperty.all(const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w800,
            )),
            side: MaterialStateProperty.all(const BorderSide(color: ThemeColors.primaryShade1)),
            shape: MaterialStateProperty.all(const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50)),
              side: BorderSide(color: ThemeColors.primaryShade1),
            )),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all(ThemeColors.light),
            backgroundColor: MaterialStateProperty.all(ThemeColors.primaryShade1),
            minimumSize: MaterialStateProperty.all(const Size(double.infinity, 56)),
            textStyle: MaterialStateProperty.all(const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w800,
            )),
            shape: MaterialStateProperty.all(const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(50)),
            )),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: ThemeConstants.inputBorder,
          enabledBorder: ThemeConstants.inputBorder,
          focusedBorder: ThemeConstants.inputFocusedBorder,
          errorBorder: ThemeConstants.inputErrorBorder,
          iconColor: ThemeColors.grayShade0,
          prefixIconColor: ThemeColors.grayShade0,
          suffixIconColor: ThemeColors.grayShade0,
          labelStyle: const TextStyle(color: ThemeColors.grayShade0),
          hintStyle: const TextStyle(color: ThemeColors.grayShade0),
          contentPadding: const EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 21,
          ),
        ),
        dropdownMenuTheme: DropdownMenuThemeData(
          menuStyle: MenuStyle(
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(11),
            )),
          ),
        ),
      ),
    );
  }
}
